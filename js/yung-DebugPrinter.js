/**
 * Convenient debug tool to print message on browser console
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {string}  LINE_SEPARATOR - line separator
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_DebugPrinter = $Class.extend({
	
	classProp : { 
		name : "com.yung.util.DebugPrinter" 
    },
	
    /**
     * maximum indent threshold
     * @member {number}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	MAX_INDENT : 40,
    
	LINE_SEPARATOR : "\n",
    
	/**
     * flag to show out of buffer
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	showOutOfBuffer : true,
    
	/**
     * flag to display null value
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	showNull : true,
    
	/**
	 * constructor
     * @memberof com_yung_util_DebugPrinter
     */
    init : function () {
        return this;
    },
    
    /** 
     * to check input if it is allowable
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {Object} input - Object for check
     * @return {Boolean} allowable flag
     */
    allowType : function (input) {
        if (typeof input == 'undefined') {
            return true;
        } else if (typeof input == 'function') {
            return false;
        } else if (typeof input == 'string') {
            return true;
        } else if (typeof input == 'number') {
            return true;
        } else if (typeof input == 'boolean') {
            return true;
        } else if (typeof input == 'object') {
            return true;
        } else if (jQuery.isArray(input)) {
            return true;
        } else {
            throw "unknown type: " + typeof input;
        }
    },
    
    /**
     * to check input if it is printable
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} input - object type
     * @param {Object} inputObj - Object for check
     * @return {Boolean} printable flag
     */
    printable : function (input, inputObj) {
        if (input == 'string') {
            return true;
        } else if (input == 'number') {
            return true;
        } else if (input == 'boolean') {
            return true;
        } else {
            if (Object.prototype.toString.call(inputObj) === '[object Date]'){
                return true;
            }
            return false;
        }
    },
    
    /**
     * print object to buffer
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} bud - String buffer to hold message
     * @param {String} indent - String for indent
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    printObjectParam : function (bud, indent, objName, obj) {
        if (indent.length > this.MAX_INDENT) {
            if (this.showOutOfBuffer) bud = bud + indent + "skip following objet ..." + this.LINE_SEPARATOR;
            return bud;
        }
        if (obj == null) {
            if (this.showNull) bud = bud + indent + "Object:" + objName + " is null";
            return bud;
        }
        if (jQuery.isArray(obj)) {
            bud = this.printArray(bud, indent, objName, obj);
        } else {
            if (this.allowType(obj) == false) {
                return bud;
            }
            var type = typeof obj;
            if (this.printable(type, obj) == true) {
                bud = bud + indent + objName + "[" + type + "] = " + obj + this.LINE_SEPARATOR;
            } else {
                bud = bud + indent + "object name:" + objName + this.LINE_SEPARATOR;
                // print field
                var start = true;
                if (start) {
                    indent = "    " + indent;
                    start = false;
                }
                for(var key in obj) {
                    var returnObj = obj[key];
                    if (returnObj == null) {
                        if (this.showNull) bud = bud + indent + key + " is null!" + this.LINE_SEPARATOR;
                        continue;
                    }
                    if (jQuery.isArray(returnObj)) {
                        bud = this.printArray(bud, indent, key, returnObj) + this.LINE_SEPARATOR;
                    } else {
                        var returnObjType = typeof returnObj;
                        if (this.printable(returnObjType, returnObj) == true) {
                            bud = bud + indent + key + "[" + returnObjType + "] = " + returnObj + this.LINE_SEPARATOR;
                        } else {
                            bud = this.printObjectParam(bud, indent, key, returnObj);
                        }
                    }
                }
            }
        }
        return bud;
    },
    
    /**
     * print Array to buffer
     *
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} bud - String buffer to hold message
     * @param {String} indent - String for indent
     * @param {String} objName - array name
     * @param {Array} array - Array to print
     */
    printArray : function (bud, indent, objName, array) {
        bud = bud + indent + "Array : " + objName + this.LINE_SEPARATOR;
        for (var i = 0; i < array.length; i++) {
            bud = this.printObjectParam(bud, indent + "    ", objName + i, array[i]);
        }
        return bud;
    },
    
    /**
     * print object to console
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    printObject : function (objName, obj) {
        if (obj == null) {
            this.printMessage(objName);
            return;
        }
        var bud = "";
        bud = this.printObjectParam(bud, "", objName, obj);
        this.printMessage(bud);
    },
    
    /**
     * print object to alert box
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    alertObject : function (objName, obj) {
        if (obj == null) {
            this.printMessage(objName);
            return;
        }
        var bud = "";
        bud = this.printObjectParam(bud, "", objName, obj);
        alert(bud);
    },
    
    /**
     * print message to console
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} message - message to print
     */
    printMessage : function (message) {
        console.log(message);
    }
});
     