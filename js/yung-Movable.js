/**
 * An easy tool to implement movable
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Movable = {
    
	classProp : { 
		name : "com.yung.util.Movable" 
	},
    
	/**
     * Callback when drag
     *
     * @callback MovableDragCallback
     * @param  {number} startX - element start left position
     * @param  {number} startY - element start top position
     * @param  {number} endX - element end left position
     * @param  {number} endY - element end top position
     */
	/**
	 * enable draggable to element
	 * 
     * @memberof com_yung_util_Movable
     * @param  {string} eleId - element id to enable movable
     * @param  {boolean} disableX - element disable movable in X-axis
     * @param  {boolean} disableY - element disable movable in Y-axis
     * @param  {object} bound - element move boundary ex. {maxX: 100, minX: 0, maxY: 100, minY: 0}
     * @param  {MovableDragCallback} callback - callback when drag
     */
	enable: function(eleId, disableX, disableY, bound, callback){
	    if (disableX == true) {
	        disableX = true;
	    } else {
	        disableX = false;
	    }
	    if (disableY == true) {
	        disableY = true;
        } else {
            disableY = false;
        }
	    if (com_yung_util_getbrowser() == 'msie') {
	        com_yung_util_Movable.draggable(document.getElementById(eleId), disableX, disableY, bound, callback);
	    } else {
	        _dragElement(document.getElementById(eleId), disableX, disableY, bound, callback);
	    }
	},
    
    /**
     * Callback for mouse event listener
     *
     * @callback draggableEventCallback
     * @param {event} e - mouse event
     */
    /** 
     * add event listener to specified element
     * 
     * @private
     * @memberof com_yung_util_Movable
     * @param  {HTMLElement} element - element to enable draggable
     * @param  {string} type - event type, ex. mousedown, mouseon
     * @param  {draggableEventCallback} callback - listener callback
     * @param  {boolean} capture - capture flag
     */
    addListener : function (element, type, callback, capture) {
        if (element.addEventListener) {
            element.addEventListener(type, callback, capture);
        } else {
            element.attachEvent("on" + type, callback);
        }
    },
    
    /** 
     * enable draggble to specified element
     * 
     * @private
     * @memberof com_yung_util_Movable
     * @param  {HTMLElement} element - element to enable draggable
     * @param  {boolean} disableX - element disable movable in X-axis
     * @param  {boolean} disableY - element disable movable in Y-axis
     * @param  {object} bound - element move boundary ex. {maxX: 100, minX: 0, maxY: 100, minY: 0}
     */
    draggable : function (element, disableX, disableY, bound, callback) {
        var dragging = null;
        if (bound == null) {
            bound = {};
        }
        com_yung_util_Movable.addListener(element, "mousedown", function (e) {
            var evt = window.event || e;
            dragging = {
                mouseX: evt.clientX,
                mouseY: evt.clientY,
                startX: parseInt(element.style.left),
                startY: parseInt(element.style.top)
            };
            bound["startX"] = evt.clientX;
            bound["startY"] = evt.clientY;
            if (element.setCapture) element.setCapture();
        });
        com_yung_util_Movable.addListener(element, "losecapture", function () {
            dragging = null;
        });
        com_yung_util_Movable.addListener(document, "mouseup", function () {
            if (document.releaseCapture) document.releaseCapture();
            if (typeof callback == 'function') {
            	if (bound["startX"] != null && bound["startY"] != null && bound["endX"] != null && bound["endY"] != null) {
            		var move = false;
            		if (disableX == false && bound["startX"] != bound["endX"]) {
            		    move = true;
            		}
            		if (disableY == false && bound["startY"] != bound["endY"]) {
                        move = true;
                    }
            		if (move == true) {
            	        // after drag
                        callback(bound["startX"], bound["startY"], bound["endX"], bound["endY"]);   
                    }
            	}
            }
            dragging = null;
            delete bound["startX"];
            delete bound["startY"];
            delete bound["endX"];
            delete bound["endY"];
        }, true);
        var dragTarget = element.setCapture ? element : document;
        com_yung_util_Movable.addListener(dragTarget, "mousemove", function (e) {
            if (!dragging) return;
            var evt = window.event || e;
            var top = dragging.startY + (evt.clientY - dragging.mouseY);
            var left = dragging.startX + (evt.clientX - dragging.mouseX);
            var posX = 0;
            var posY = 0;
            if (disableY == false) {
                posY = Math.max(0, top);
                if (bound != null) {
                    if (bound["maxY"] != null && posY > bound["maxY"]) {
                        posY = bound["maxY"];
                    }
                    if (bound["minY"] != null && posY < bound["minY"]) {
                        posY = bound["minY"];
                    }
                }
                element.style.top = posY + "px";
            }
            if (disableX == false) {
                posX = Math.max(0, left);
                if (bound != null) {
                    if (bound["maxX"] != null && posX > bound["maxX"]) {
                        posX = bound["maxX"];
                    }
                    if (bound["minX"] != null && posX < bound["minX"]) {
                        posX = bound["minX"];
                    }
                }
                element.style.left = posX + "px";
            }
            bound["endX"] = evt.clientX;
            bound["endY"] = evt.clientY;
        }, true);
    }
};

function _dragElement(elmnt, disableX, disableY, bound, callback) {
    if (disableX != true) {
        disableX = false;
    }
    if (disableY != true) {
        disableY = false;
    }
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    var beginX = 0, beginY = 0, finishX = 0, finishY = 0
    //if (document.getElementById(elmnt.id + "header")) {
    //    /* if present, the header is where you move the DIV from: */
    //    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    //} else {
        /* otherwise, move the DIV from anywhere inside the DIV: */
        elmnt.onmousedown = dragMouseDown;
    //}

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        beginX = e.clientX;
        pos4 = e.clientY;
        beginY = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        var posX = 0;
        var posY = 0;
        if (disableY == false) {
            posY = elmnt.offsetTop - pos2;
            if (bound != null) {
                if (bound["maxY"] != null && posY > bound["maxY"]) {
                    posY = bound["maxY"];
                }
                if (bound["minY"] != null && posY < bound["minY"]) {
                    posY = bound["minY"];
                }
            }
            elmnt.style.top = posY + "px";
        }
        if (disableX == false) {
            posX = elmnt.offsetLeft - pos1;
            if (bound != null) {
                if (bound["maxX"] != null && posX > bound["maxX"]) {
                    posX = bound["maxX"];
                }
                if (bound["minX"] != null && posX < bound["minX"]) {
                    posX = bound["minX"];
                }
            }
            elmnt.style.left = posX + "px";
        }
        finishX = e.clientX;
        finishY = e.clientY;
    }

    function closeDragElement(e) {
        /* stop moving when mouse button is released: */
        document.onmouseup = null;
        document.onmousemove = null;
        if (typeof callback == 'function') {
            callback(beginX, beginY, finishX, finishY);
        }
    }
}

$Y.reg(com_yung_util_Movable);  