/**
 * HTML Audio tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AudioRecorder = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.AudioRecorder" 
    },
    
    /**
     * Media recorder object
     * @member {object}
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    mediaRecorder : null,
    
    /**
     * Media record data bytes
     * @member {Array}
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    recordedChunks : null,
    
    /**
     * Div id to put HTML player bar
     * @member {string}
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    playerDivId : null,
    
    /**
     * Media recorder controller
     * @member {object}
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    mediaControl : null,
    
    /**
     * constructor
     * @memberof com_yung_util_AudioRecorder
     * @param  {string} playerDivId - div id to put player bar
     */
    init : function(playerDivId) {
        if (!navigator.getUserMedia && !navigator.webkitGetUserMedia && !navigator.mozGetUserMedia && !navigator.msGetUserMedia) {
            alert('User Media API not supported.');
            return null;
        }
        var constraints = {};
        constraints['audio'] = true;
        constraints['_instance'] = this;
        this.getUserMedia(constraints, function(stream) {
            constraints["_instance"].mediaRecorder = new MediaRecorder(stream, constraints);
            constraints["_instance"].mediaRecorder.ondataavailable = function (event) {
                if (event.data.size > 0) {
                    constraints["_instance"].recordedChunks.push(event.data);
                } else {
                    alert("audio is empty!");
                }
            };
        }, function(err) {
            alert('Error: ' + err.name + ", " + err.message);
        });
        this.recordedChunks = [];
        this.playerDivId = playerDivId;
        this.mediaControl = null;
        if (this.playerDivId != null) {
            var mediaCtrl = jQuery("#" + this.playerDivId + " audio")[0];
            if (mediaCtrl == null) {
                jQuery("#" + this.playerDivId).html('<audio controls><source src="" type="audio/mpeg"/></audio>');
            }
            this.mediaControl = jQuery("#" + this.playerDivId + " audio")[0];
        }
        return this;
    },
    
    getUserMedia : function (options, successCallback, failureCallback) {
        var api = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
        if (api) {
            return api.bind(navigator)(options, successCallback, failureCallback);
        }
    },
    
    /** 
     * start record action
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    startRecord : function () {
        this.recordedChunks = [];
        this.mediaRecorder.start();
    },
    
    /** 
     * stop record action
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    stopRecord : function () {
        this.mediaRecorder.stop();
        if (this.playerDivId != null) {
            var self = this;
            setTimeout(function () {
                var superBuffer = new Blob(self.recordedChunks);
                self.mediaControl.src = window.URL.createObjectURL(superBuffer);
                jQuery("#" + self.playerDivId).css("display", "block");
            }, 100);
        }
    },
    
    /** 
     * play record action
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    playRecord : function () {
        if (this.playerDivId == null) {
            alert("please define player div id!");
            return;
        }
        jQuery("#" + this.playerDivId).css("display", "block");
    },
    
    /** 
     * download media file
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    downloadRecord : function () {
        var blob = new Blob(this.recordedChunks, {
			type : 'audio'
		});
		var url = URL.createObjectURL(blob);
		var a = document.createElement('a');
		document.body.appendChild(a);
		a.style = 'display: none';
		a.href = url;
		a.download = 'audio.mp3';
		a.click();
		window.URL.revokeObjectURL(url);
    },
    
    
    /**
     * Callback for after upload response 
     *
     * @callback audioAfterSend
     * @param {object} response - response data
     * @param {com_yung_util_AjaxHttp} self - this com_yung_util_AjaxHttp
     * @param {object} caller - caller which pass by send method
     */
    /** 
     * upload media file
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     * @param  {string} formId - upload form id
     * @param  {string} base64DataId - input id to store base64 data
     * @param  {string} nameId - input id for upload file name
     * @param  {actionUrl} nameId - form action url
     * @param  {audioAfterSend} afterSend - a callback after upload
     */
    uploadRecord : function (formId, base64DataId, nameId, name, actionUrl, afterSend) {
        var blob = new Blob(this.recordedChunks, {
			type : 'audio'
		});
		var reader = new FileReader();
        reader.readAsDataURL(blob); 
        reader.onloadend = function() {
        	var dataUrl = reader.result;
            var base64data = dataUrl.split(',')[1];
            jQuery("#" + base64DataId).val(base64data);
            jQuery("#" + nameId).val(name);
            var ajaxhttp = new com_yung_util_AjaxHttp(actionUrl, formId, function callback(response, self, caller)
            {
                var errormsg = response.errormsg;
                if(errormsg != ''){
                    alert(errormsg);
                    return;
                } else {
                	if (typeof afterSend == 'function') {
                		afterSend(response, self, caller);
                	} else {
                		alert("Upload successfully!");
                	}
                }
            });
            ajaxhttp.send();
       }
    }
});

jQuery( document ).ready(function() {
    if (typeof com_yung_util_AjaxHttp == 'undefined') {
        alert("yung-AudioRecorder.js requires yung-AJAX.js!");
    }
});   