/**
 * An easy tool create popup iframe
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Popup = $Class.extend({
	
	classProp : { 
    	name : "com.yung.util.Popup" 
    },
    
    color : "rgb(204, 216, 231)",
    
    /**
     * Index to identified iframe window
     * @member {number}
     * @instance
     * @memberof com_yung_util_Popup
     */
    currentPopupIndex : null,
    currentWidth : 0,
    currentHeight : 0,
    enableFixScorll : true,
    
    /**
     * a flag to decide whether can close iframe window
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_Popup
     */
    canCloseWindow : null,
    
    /**
	 * constructor, private used only
	 * Note: do not use this constructor to create instance.
	 * Use com.yung.util.Popup.instance()
	 * 
	 * @private
     * @memberof com_yung_util_Popup
     */
    init : function () {
        if (yung_global_var["com_yung_util_Popup"] != null) {
            throw "Please use com.yung.util.Popup.instance() to create instance!";
        }
        this.currentPopupIndex = 0;
        this.canCloseWindow = true;
        com_yung_util_Popup.instance(this);
        // follow use only for old IE
        var browser = com_yung_util_getbrowser();
        if (browser == 'msie') {
            this.enableFixScorll = false;
            var self = this;
            jQuery(window).scroll(function(){
                if (self.enableFixScorll == false) {
                    return;
                }
                var holder = document.getElementById('popup-holder-' + self.currentPopupIndex);
                if (holder == null) {
                    return;
                }
                jQuery("#popup-holder-" + self.currentPopupIndex).css("display", "none");
            });
            jQuery(window).scrollEnd(function(){
                if (self.enableFixScorll == false) {
                    return;
                }
                var holder = document.getElementById('popup-holder-' + self.currentPopupIndex);
                if (holder == null) {
                    return;
                }
                var position = new com_yung_util_Position(document);
                var pos = position.getCenterPosition(self.currentWidth, self.currentHeight);
                var top = pos.top;
                var left = pos.left;
                jQuery("#popup-holder-" + self.currentPopupIndex).css("top", top + "px");
                jQuery("#popup-holder-" + self.currentPopupIndex).css("left", left + "px");
                jQuery("#popup-holder-" + self.currentPopupIndex).css("display", "");
            }, 150);    
        }
        return this;
    },
    
    /** 
     * set popup color
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} color - popup window color
     */
    setColor : function (color) {
    	if (_validType('string', color) == false) {
    		throw "color is not string type";
    	}
    	if (color != null && color != '') {
    		this.color = color;
        }
    },
    
    /** 
     * create popup iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     */
    createPopoupFrame : function (index) {
        var popup = jQuery("#popup-background-" + index)[0];
        if (popup == null) {
            var obj = {};
            obj["index"] = index;
            var template = new com.yung.util.BasicTemplate(obj);
            template.add('<div id="popup-background-{{index}}" style="display: none; position: absolute; left: 0px; top: 0px; width: 100%; height: 2000px; z-index: 10{{index}}00; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);" ></div>');
            template.add('<div id="popup-holder-{{index}}" class="dialog-border" style="display: none; position: absolute; left: 0px; top: 0px; border-radius: 3px; background-color: ' + this.color + '; z-index: 10{{index}}01; width: 200px; height: 100px; box-shadow: 5px 5px 5px #d7d7d7;" >');
            template.add('    <div id="popup-titlebar-{{index}}" class="dialog-titlebar" style="cursor: move; background-color: ' + this.color + ';" >');
            template.add('        <div id="popup-title-{{index}}" style="float: left; padding-left: 18px; padding-top: 3px; "></div><div style="height: 18px;" class="dialog-closeButton" onclick="com_yung_util_Popup.instance().closePopup()"><div class="toolButton"><span style="color: red;" title="close">&#10006;</span></div></div>');
            template.add('    </div>');
            template.add('    <div id="iframe-holder-{{index}}" style="margin: 5px; display: none; background: white;" height="100%">');
            template.add('        <iframe id="popup-content-{{index}}" src="" width="99%" frameborder="0" style="height: 100%; border-style: none transparent;" onload="com_yung_util_Popup.instance().showIframe({{index}});"></iframe>');
            template.add('    </div>');
            template.add('</div>');
            var html = template.toHtml();
            jQuery("body").append(html);
            com.yung.util.Movable.enable("popup-holder-" + index);
	    }
    },
    
    /** 
     * set canClose flag
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} canClose - a flag to decide whether can close iframe window
     */
    setCanCloseWindow : function (canClose) {
        this.canCloseWindow = canClose;
    },
    
    /** 
     * show popup iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @param  {boolean} show - show iframe or not
     */
    showIframe : function (index, show) {
        if (show == false) {
            jQuery("#iframe-holder-" + index).css("display", "none");
            return;    
        }
        jQuery("#iframe-holder-" + index).css("display", "block");
    },
    
    /**
     * Callback when close popup
     *
     * @callback closePopupCallback
     */
    /** 
     * close[hide] popup iframe
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @param  {closePopupCallback} callback - callback when close popup
     */
    closePopup : function (index, callback) {
        if (this.canCloseWindow) {
            if (index == null) {
                index = this.currentPopupIndex;
            }
            var win = this.getIframeWin();
            if (win != null && typeof win["closePopupBefore"] == "function") {
                var ret = win["closePopupBefore"]();
                if (ret == false) {
                    return;
                }
            }
            this.showIframe(index, false);
            var bg = document.getElementById('popup-background-' + index);
            if (bg != null) {
                jQuery("#popup-background-" + index).css("display", "none");
                jQuery("#popup-holder-" + index).css("display", "none");
                this.currentPopupIndex = this.currentPopupIndex - 1;
            }
            if (this.currentPopupIndex < 0) {
                this.currentPopupIndex = 0;
            }
            if (typeof callback === "function") {
                callback();
            }
        } else {
            alert("Can not close window now!");
        }
    },
    
    /** 
     * open popup iframe, position and index are optional.
     * if position is empty, use center position of window.
     * if index is empty, use currentPopupIndex plus one[next popup]
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} title - popup window title
     * @param  {number} width - popup window width
     * @param  {number} height - popup window height
     * @param  {string} url - popup iframe url
     * @param  {object} pos - position for popup iframe [optional]
     * @param  {number} index - index of popup iframe [optional]
     */
    openPopup : function (title, width, height, url, pos, index) {
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        var widthStr = width + "";
        var heightStr = height + "";
        if (widthStr.indexOf('%') > 0) {
            var idx = widthStr.indexOf('%');
            var popWidthPercent = widthStr.substring(0, idx) * 1.0;
            if (isNaN(popWidthPercent)) {
                alert(popWidthPercent + " is not number!");
            }
            width = windowWidth * popWidthPercent / 100;
        }
        if (heightStr.indexOf('%') > 0) {
            var idx = heightStr.indexOf('%');
            var popHeightPercent = heightStr.substring(0, idx) * 1.0;
            if (isNaN(popHeightPercent)) {
                alert(popHeightPercent + " is not number!");
            }
            height = windowHeight * popHeightPercent / 100;
        }
        this.currentWidth = width;
        this.currentHeight = height;
        var browser = com_yung_util_getbrowser();
        if (pos == null) {
            var position = new com_yung_util_Position(document);
            if (this.enableFixScorll == true && browser != 'msie') {
                pos = position.getScreenCenterPosition(width, height); 
            } else {
                pos = position.getCenterPosition(width, height);
            }
        }
        this.openPopupWithPos(title, width, height, url, pos, index);
    },
    
    /** 
     * open popup iframe, position and index are optional.
     * if position is empty, use center position of window.
     * if index is empty, use currentPopupIndex plus one[next popup]
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} title - popup window title
     * @param  {number} width - popup window width
     * @param  {number} height - popup window height
     * @param  {string} url - popup iframe url
     * @param  {object} pos - position for popup iframe [optional]
     * @param  {number} index - index of popup iframe [optional]
     */
    openPopupWithPos : function (title, width, height, url, pos, index) {
        if (index == null) {
            this.currentPopupIndex = this.currentPopupIndex + 1;
            index = this.currentPopupIndex;
        }
        this.createPopoupFrame(index);
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        var maskHeight = windowHeight;
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        } else {
            maskHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        if (pos == null) {
            var position = new com_yung_util_Position(document);
            pos = position.getCenterPosition(width, height);    
        }
        var top = pos.top;
        var left = pos.left;
        var browser = com_yung_util_getbrowser();
        if (this.enableFixScorll == true && browser != 'msie') {
            jQuery("#popup-holder-" + index).css("position", "fixed");
        } else {
            jQuery("#popup-holder-" + index).css("position", "absolute");
        }
        jQuery("#popup-holder-" + index).css("top", top + "px");
        jQuery("#popup-holder-" + index).css("left", left + "px");
        jQuery("#popup-title-" + index).html(title);
        jQuery("#popup-holder-" + index).css("height", height + "px");
        jQuery("#popup-holder-" + index).css("width", width + "px");
        jQuery("#popup-content-" + index).css("height", (height - 50) + "px");
        jQuery("#popup-content-" + index).attr("src", url);
        jQuery("#popup-background-" + index).css("display", "block");
        jQuery("#popup-background-" + index).css("height", maskHeight + "px");
        jQuery("#popup-background-" + index).css("width", windowWidth + "px");
        jQuery("#popup-holder-" + index).css("display", "block");
    },
    
    /** 
     * get iframe window by index, if index is empty, use currentPopupIndex
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @return  {window} iframe window
     */
    getIframeWin : function (index) {
        if (index == null) {
            index = this.currentPopupIndex;
        }
        var ele = document.getElementById("popup-content-" + index);
        if (ele == null) {
            return null;
        }
        var win = document.getElementById("popup-content-" + index).contentWindow;
        return win;
    },
    
    /** 
     * get parent iframe window
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @return  {window} iframe window
     */
    getParentWin : function () {
        var win = this.getIframeWin(this.currentPopupIndex - 1);
        if (win == null) {
            return window;
        } else {
            return win;
        }
    },
    
    /** 
     * search parent iframe window iteratively until find partial url string. 
     * if not found return null
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} partialUrl - partial url
     * @return  {window} iframe window
     */
    getParentWinByUrl : function (partialUrl) {
    	var nextIdx = this.currentPopupIndex - 1;
    	while (true) {
    		if (nextIdx < 0) {
    			break;
    		}
    		var win = this.getIframeWin(nextIdx);
    		if (win == null) {
    			break;
    		}
    		if (win.location.href.indexOf(partialUrl) >= 0) {
    			return win;
    		}
    		nextIdx = nextIdx -1;
    	}
    	if (window.location.href.indexOf(partialUrl) >= 0) {
    		return window;
    	}
    	return null;
    },
    
    /** 
     * To Fix popup position when scroll window
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {boolean} enableFixScorll - fix or not
     */
    setEnableFixScroll : function (enableFixScorll) {
        if (enableFixScorll == true) {
            this.enableFixScorll = true;
        } else {
            this.enableFixScorll = false;
        }
    }
    
});

/** 
 * get com_yung_util_Popup global instance,
 * com_yung_util_Popup instance is global unique.
 * 
 * @param  {com_yung_util_Popup} popup - com_yung_util_Popup instance, for private use only
 * @return  {com_yung_util_Popup} com_yung_util_Popup instance
 */
com_yung_util_Popup.instance = function(popup) {
    if (popup != null) {
        if (popup instanceof com_yung_util_Popup) {
            yung_global_var["com_yung_util_Popup"] = popup;
        }
    } else {
        try {
            if (window.parent.yung_global_var != null && window.parent.yung_global_var["com_yung_util_Popup"] != null) {
                return window.parent.yung_global_var["com_yung_util_Popup"];
            }    
        } catch (e) {
            // prevent permission denied
        }
        var instance = yung_global_var["com_yung_util_Popup"];
        if (instance == null) {
            instance = new com_yung_util_Popup();
        }
        return instance;
    }
}
jQuery( document ).ready(function() {
    if (typeof com_yung_util_Movable == 'undefined') {
        alert("yung-Popup.js requires yung-Movable.js!");
    }
    if (typeof com_yung_util_BasicTemplate == 'undefined') {
        alert("yung-Popup.js requires yung-BasicTemplate.js!");
    }
    if (typeof com_yung_util_Position == 'undefined') {
        alert("yung-Popup.js requires yung-Position.js!");
    }
});