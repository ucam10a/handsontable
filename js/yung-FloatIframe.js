/**
 * Float iframe element, usually used for display complex message, like report, table.
 * Note: float iframe will not create mask to block screen. 
 * if mask is required, use com_yung_util_Popup
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_FloatIframe = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.FloatIframe" 
    },
    
    hashId : '',
    
    /**
     * float iframe div id
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    divId : null,
    
    /**
     * constructor
     * @memberof com_yung_util_FloatIframe
     * @param  {string} divId - float iframe div id
     * @param  {boolean} onblurClose - a flag to close float iframe when onblur
     */
    init : function (divId, onblurClose) {
        $Class.validate(this.classProp.name, divId);
        this.divId = divId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + divId);
        this.createFloatIFrame();
        if (onblurClose == true) {
            this.onblurClose();
        }
        return this;
    },
    
    /** 
     * close float iframe when onblur
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    onblurClose : function () {
        var element = jQuery("body");
        var event = "click";
        var self = this;
        element.bind(event, function() {
            self.closeFloatIframe();
        });
        this.showCloseButton(false);
    },
    
    /** 
     * show close button in div
     * 
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {boolean} show - show or not
     */
    showCloseButton : function (show) {
        if (show == true) {
            jQuery("#yung-FloatIframe-closeBtn-" + this.divId).css('display', '');
        } else {
            jQuery("#yung-FloatIframe-closeBtn-" + this.divId).css('display', 'none');
        }
    },
    
    /** 
     * create iframe content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    createFloatIFrame : function () {
        var floatDiv = jQuery("#yung-FloatIframe-" + this.divId)[0];
        if (floatDiv == null) {
            var obj = {};
            obj["divId"] = this.divId;
            var template = new com.yung.util.BasicTemplate(obj);
            template.add('<div id="yung-FloatIframe-{{divId}}" class="float-border" style="position: absolute; left: 0px; top: 0px; margin: 3px; display: none; background: white; box-shadow: 5px 5px 5px #d7d7d7; width: 500px; width:300px; z-index: 5000;">');
            template.add('    <div id="yung-FloatIframe-loading-{{divId}}" style="width: 100%; height: 100%; display: block;">');
            template.add('        <table width="100%" height="100%">');
            template.add('            <tr>');
            template.add('                <td align="center"><div class="loader">Loading...</div></td>');
            template.add('            </tr>');
            template.add('        </table>');
            template.add('    </div>');
            template.add('    <div id="yung-FloatIframe-container-{{divId}}" style="display: none; width: 100%; height: 100%;">');
            template.add('        <div id="yung-FloatIframe-closeBtn-{{divId}}" style="position: absolute; right: 2px; top: 0px; cursor: pointer;"><span style="color: red; background-color: white;" title="close" onclick="fireInstanceMethod(\'' + this.hashId + '\', \'showFloatIframe\', false);" style="cursor: pointer;" >&#10006;</span></div>');
            template.add('        <iframe id="yung-FloatIframe-content-{{divId}}" src="" width="100%" frameborder="0" style="height: 100%; border-style: none transparent;" onload="fireInstanceMethod(\'' + this.hashId + '\', \'showFloatContent\');"></iframe>');
            template.add('    </div>');
            template.add('</div>');
            var html = template.toHtml();
            jQuery("body").append(html);
        } else {
            throw "div id: " + this.divId + " element exist, please use another one.";
        }
    },
    
    /** 
     * show float content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    showFloatContent : function () {
        jQuery("#yung-FloatIframe-loading-" + this.divId).css("display", "none");
        jQuery("#yung-FloatIframe-container-" + this.divId).css("display", "block");
    },
    
    /** 
     * show float iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {boolean} show - show flag
     */
    showFloatIframe : function (show) {
        var divId = this.divId;
        if (show == false) {
            var display = jQuery("#yung-FloatIframe-" + divId).css("display");
            if (display != 'none') {
                jQuery("#yung-FloatIframe-" + divId).css("display", "none");
            }
            jQuery("yung-FloatIframe-loading" + divId).css("display", "block");
            return; 
        }
        var display = jQuery("#yung-FloatIframe-" + divId).css("display");
        if (display != 'block') {
            jQuery("#yung-FloatIframe-" + divId).css("display", "block");
        }
    },
    
    /**
     * Callback when close float iframe 
     *
     * @callback floatIframeCloseCallback
     */
    /** 
     * close and hide iframe
     * 
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {floatIframeCloseCallback} callback - callback when close float iframe 
     */
    closeFloatIframe : function (callback) {
        this.showFloatIframe(false);
        if (typeof callback === "function") {
            callback();
        }   
    },
    
    /** 
     * open float iframe, width and height are optional.
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} url - url for float iframe
     * @param  {object} pos - position object, include top and left number
     * @param  {number} width - float iframe width
     * @param  {number} height - float iframe height
     */
    openFloatIframe : function (url, pos, width, height) {
        if (width == null) {
            width = 500;
        }
        if (height == null) {
            height = 300;
        }
        if (pos == null) {
            alert("please specify position!");
        }
        var top = pos.top + "px";
        var left = pos.left + "px";
        jQuery("#yung-FloatIframe-" + this.divId).css("width", width);
        jQuery("#yung-FloatIframe-" + this.divId).css("height", height);
        jQuery("#yung-FloatIframe-" + this.divId).css("left", left);
        jQuery("#yung-FloatIframe-" + this.divId).css("top", top);
        document.getElementById('yung-FloatIframe-content-' + this.divId).src = url;
        // use setTimeout to prevent body click event conflict
        var self = this;
        setTimeout(function(){ 
            self.showFloatIframe(true); 
        }, 300);
    }
});

/** 
 * get com_yung_util_FloatIframe global instance,
 * 
 * @param  {string} divId - div id
 * @param  {boolean} onblurClose - a flag to close float div when onblur
 * @return  {com_yung_util_FloatIframe} com_yung_util_FloatIframe instance
 */
com_yung_util_FloatIframe.instance = function(divId, onblurClose) {
    return $Class.getInstance("com.yung.util.FloatIframe", divId, onblurClose);
}

jQuery( document ).ready(function() {
    if (typeof com_yung_util_BasicTemplate == 'undefined') {
        alert("yung-FloatIframe.js requires yung-BasicTemplate.js!");
    }
});