/**
 * Create tab tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Tab = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.Tab" 
    },
    
    hashId : '',
    
    /**
     * div id to put tab
     * @member {string}
     * @instance
     * @memberof com_yung_util_Tab
     */
    divId : null,
    
    /**
     * tab title array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_Tab
     */
    titles : null,
    
    /**
     * tab color
     * @member {string}
     * @instance
     * @memberof com_yung_util_Tab
     */
    color : null,
    
    /**
     * tab width
     * @member {number}
     * @instance
     * @memberof com_yung_util_Tab
     */
    width : null,
    
    /**
     * tab span
     * @member {number}
     * @instance
     * @memberof com_yung_util_Tab
     */
    span : null,
    
    /**
     * tab unique id to identify
     * @member {Array}
     * @instance
     * @memberof com_yung_util_Tab
     */
    tabId : null,
    
    srcContent : null,
    
    /**
     * Callback when click tab
     *
     * @callback clickTabCallback
     * @param {number} tabIndex - tab index
     */
    /**
     * tab click callback
     * @member {clickTabCallback}
     * @instance
     * @memberof com_yung_util_Tab
     */
    callback : null,
    
    /**
     * Callback when create tab
     *
     * @callback createTabCallback
     */
    /**
     * constructor
     * @memberof com_yung_util_Tab
     * @param  {string} divId - div id to put tab
     * @param  {Array} titles - tab title array
     * @param  {string} tabId - tab unique id to identify, if empty, system will create one
     * @param  {string} color - tab color, if empty, it will be black
     * @param  {number} width - tab width, if empty, it will be 120
     * @param  {number} span - tab span, if empty, it will be 30
     * @param  {createTabCallback} afterRender - data object to render HTML
     * @param  {number} afterRenderTime - wait millisecond to run afterRender
     */
    init : function (divId, titles, tabId, color, width, span, afterRender, afterRenderTime) {
        $Class.validate(this.classProp.name, divId);
        if (divId == null) {
            throw "Please specify divId";
        }
        this.divId = divId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + divId);
        if (titles == null) {
            throw "Please specify titles";
        }
        if (jQuery.isArray(titles) == false) {
            throw "titles has to be array";
        }
        this.titles = titles;
        if (color == null || color == '') {
            this.color = "black";
        } else {
            this.color = color;
        }
        if (tabId == null || tabId == '') {
            this.tabId = new Date().getTime() + "";
        } else {
            this.tabId = tabId;
        }
        if (width == null || width == '') {
            this.width = 120;
        } else {
            this.width = width;
        }
        if (span == null || span == '') {
            this.span = 30;
        } else {
            this.span = span;
        }
        this.srcContent = new com_yung_util_BasicList("string");
        this.createTab();
        for (var i = 0; i < this.srcContent.size(); i++) {
            this.setTabContent(i, this.srcContent.get(i));
        }
        this.showTab(0);
        var waitTime = 250;
        if (typeof afterRenderTime == 'number') {
            waitTime = afterRenderTime;
        }
        if (typeof afterRender == 'function') {
            setTimeout(function () {
                afterRender();
            }, waitTime);
        }
        return this;
    },
    
    /** 
     * create tab element
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     */
    createTab : function () {
        this.getSourceTabContent();
        var headBasicTmp = new com.yung.util.BasicTemplate();
        headBasicTmp.add('<td width="{{width}}">');
        headBasicTmp.add('    <div id="{{tabId}}Tab{{idx}}Show" style="display:none; border-color: {{tabColor}}; background-color: {{tabColor}}; color: white; white-space: nowrap; text-align: center; border-style: solid; border-color: {{tabColor}}; border-width: 2px 1px 0px 1px; padding-top: 5px; padding-bottom: 5px; cursor: pointer;" >');
        headBasicTmp.add('        <span>{{title}}</span>');
        headBasicTmp.add('    </div>');
        headBasicTmp.add('    <div id="{{tabId}}Tab{{idx}}Wait" style="display:none; border-color: {{tabColor}}; background-color: white; color: {{tabColor}}; white-space: nowrap; text-align: center; border-style: solid; border-color: {{tabColor}}; border-width: 2px 2px 0px 2px; padding-top: 5px; padding-bottom: 5px; cursor: pointer;" onclick="fireInstanceMethod(\'' + this.hashId + '\', \'showTab\', {{idx}});">');
        headBasicTmp.add('        <span>{{title}}</span>');
        headBasicTmp.add('    </div>');
        headBasicTmp.add('    </div>');
        headBasicTmp.add('</td>');
        headBasicTmp.add('<td width="{{span}}" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
        var headTmp = new com.yung.util.ArrayTemplate();
        headTmp.setBasicTmp(headBasicTmp);
        var contentBasicTmp = new com.yung.util.BasicTemplate();
        contentBasicTmp.add('<div id="{{tabId}}TabContent{{idx}}" style="display:none; border-top: 1px solid {{tabColor}}; padding-left: 10px; padding-right: 10px;">');
        contentBasicTmp.add('</div>');
        var contentTmp = new com.yung.util.ArrayTemplate();
        contentTmp.setBasicTmp(contentBasicTmp);
        var dataArray = this.getDataArray();
        var mainTmp = new com.yung.util.BasicTemplate();
        mainTmp.add('<table border="0" cellpadding="0" cellspacing="0" width="100%">');
        mainTmp.add('    <tr>');
        mainTmp.add('        <td width="15" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
        mainTmp.add('        {{headTmp}}');
        mainTmp.add('        <td id="{{tabId}}FinalTd" width="*" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
        mainTmp.add('    </tr>');
        mainTmp.add('    <tr>');
        mainTmp.add('        <td colspan="{{colspan}}">');
        mainTmp.add('            {{contentTmp}}');
        mainTmp.add('        </td>');
        mainTmp.add('    </tr>');
        mainTmp.add('</table>');
        var mainData = {};
        mainData["colspan"] = (this.titles.length * 2) + 2;
        mainData["tabColor"] = this.color;
        mainData["tabId"] = this.tabId;
        mainData["headTmp"] = headTmp.toHtml(dataArray);
        mainData["contentTmp"] = contentTmp.toHtml(dataArray);
        jQuery("#" + this.divId).html(mainTmp.toHtml(mainData));
        var browser = com_yung_util_getbrowser();
        if (browser == 'msie') {
            var self = this;
            setTimeout(function(){
                self.adjustRightWidth(self);
            }, 500);
        }
    },
    
    /** 
     * adjust tab width
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {com_yung_util_Tab} tab - tab instance
     */
    adjustRightWidth : function (tab) {
        var divWidth = jQuery("#" + tab.divId).width();
        var leftWidth = tab.titles.length * (tab.width + tab.span) + 15;
        var rightWidth = divWidth - leftWidth;
        if (rightWidth > 0) {
            jQuery("#" + tab.tabId + "FinalTd").attr("width", rightWidth);
        }
    },
    
    /** 
     * get render tab data array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @return  {Array} data array
     */
    getDataArray : function () {
        var dataArray = [];
        for (var i = 0; i < this.titles.length; i++) {
            var data = {};
            data["title"] = this.titles[i];
            data["tabId"] = this.tabId;
            data["tabColor"] = this.color;
            data["length"] = this.titles.length;
            data["idx"] = i + "";
            data["width"] = this.width + "";
            data["span"] = this.span + "";
            dataArray.push(data);
        }
        return dataArray;
    },
    
    /** 
     * show specified tab
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     */
    showTab : function (idx) {
        for ( var i = 0; i < this.titles.length; i++) {
            if (idx == i) {
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("display", "block");
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("background-color", this.color);
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("color", "white");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("display", "none");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("background-color", "");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("color", this.color);
                jQuery("#" + this.tabId + "TabContent" + i).css("display", "block");
            } else {
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("display", "none");
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("background-color", "");
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("color", this.color);
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("display", "block");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("background-color", "");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("color", this.color);
                jQuery("#" + this.tabId + "TabContent" + i).css("display", "none");
            }
        }
        if (typeof this.callback == 'function') {
            this.callback(idx);
        }
    },
    
    /** 
     * get tab source default HTML code
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @return  {string} source default HTML code
     */
    getSourceTabContent : function () {
        var self = this;
        var found = false;
        jQuery("#" + this.divId + " .yungTabContent").each(function(index) {
            var html = jQuery(this).html();
            self.srcContent.add(html);
            found = true;
        });
        if (found == false) {
            // probably nested tab
            jQuery("#" + this.divId + " .yungTabContent_" + self.tabId).each(function(index) {
                var html = jQuery(this).html();
                self.srcContent.add(html);
                found = true;
            });
        }
    },
    
    /** 
     * set tab content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     * @param  {string} html - tab content
     */
    setTabContent : function (idx, html) {
        jQuery("#" + this.tabId + "TabContent" + idx).html(html);
    },
    
    /** 
     * set tab title
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     * @param  {string} title - tab title
     */
    setTabTitle : function (idx, title) {
    	jQuery("#" + this.tabId + "Tab" + idx + "Show").html("<span>" + title + "</span>");
    	jQuery("#" + this.tabId + "Tab" + idx + "Wait").html("<span>" + title + "</span>");
    },
    
    /** 
     * set tab click callback
     * 
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {clickTabCallback} callbackFunct - tab click callback
     */
    setCallback : function (callbackFunct) {
        if (typeof callbackFunct == 'function') {
            this.callback = callbackFunct;
        } else {
            alert('callbackFunct is not function!');
        }
    }
});

com_yung_util_Tab.instance = function (divId, titles, tabId, color, width, span, afterRender, afterRenderTime) {
    return $Class.getInstance("com.yung.util.Tab", divId, titles, tabId, color, width, span, afterRender, afterRenderTime);
}

jQuery( document ).ready(function() {
    if (typeof com_yung_util_BasicTemplate == 'undefined') {
        alert("yung-Tab.js requires yung-BasicTemplate.js!");
    }
});