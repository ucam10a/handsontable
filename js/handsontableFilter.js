/**
 * handsontable filter
 * 
 */
var com_yung_handsontable_HandsontableFilter = $Class.extend({
    classProp : { name : "com.yung.handsontable.HandsontableFilter"},
    hsontUtil : null,
    id : null,
    filterDiv : null,
    srcData : null,
    filterData : null,
    template : null,
    optTemplate : null,
    colIdx : null,
    init : function (hsontUtil) {
        var allow = hsontUtil instanceof com.yung.handsontable.HandsontableUtil;
        if (allow == false) {
            throw "argument type is not com.yung.handsontable.HandsontableUtil";
        }
        this.hsontUtil = hsontUtil;
        var hotContainer = hsontUtil.container;
        this.id = jQuery(hotContainer).attr("id");
        this.filterDiv = com.yung.util.FloatDiv.instance('filter', false, 'border-style: solid; border-width: 1px;');
        var _filterDiv = this.filterDiv;
        jQuery(hotContainer).bind("click", function() {
            _filterDiv.closeFloatDiv();
        });
        this.filterDiv.showCloseButton(true);
        this.srcData = {};
        this.filterData = {};
        com_yung_handsontable_HandsontableFilter.instance(this);
        var templateData = {};
        templateData["id"] = this.id;
        this.template = new com.yung.util.BasicTemplate(templateData);
        this.template.add('<table style="width: 96%; height: 100%">');
        this.template.add('    <tr>');
        this.template.add('        <td style="cursor: pointer;"><span onclick="com_yung_handsontable_HandsontableFilter.instance(\'{{id}}\').sort({{colIdx}}, \'asc\');">Sort Ascending &uarr; </span></td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td style="cursor: pointer;"><span onclick="com_yung_handsontable_HandsontableFilter.instance(\'{{id}}\').sort({{colIdx}}, \'desc\');">Sort Descending &darr; </span></td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td><input id="handsontable-filter-{{colIdx}}" type="text" value="" onkeyup="com_yung_handsontable_HandsontableFilter.instance(\'{{id}}\').filterOptions(this);" /></td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td>');
        this.template.add('            <select id="handsontable-filter-condition-{{colIdx}}" onchange="com_yung_handsontable_HandsontableFilter.instance(\'{{id}}\').filterOptions();" >');
        this.template.add('                <option value="include">include</option>');
        this.template.add('                <option value="exclude">exclude</option>');
        this.template.add('                <option value="equal">equal</option>');
        this.template.add('                <option value="not equal">not equal</option>');
        this.template.add('                <option value="greater">greater</option>');
        this.template.add('                <option value="less">less</option>');
        this.template.add('            </select>');
        this.template.add('        </td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td> <a href="javascript:void(null);" onclick="com_yung_handsontable_HandsontableFilter.instance(\'{{id}}\').checkAll();" >Check All</a> <a href="javascript:void(null);" onclick="com_yung_handsontable_HandsontableFilter.instance(\'{{id}}\').uncheckAll();" >UnCheck All</a> </td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td style="border-style: solid; border-width: 1px; border-radius: 2px; border-color: grey;" >');
        this.template.add('            <div style="width: 99%; height: 130px; overflow-x: hidden;" >{{optTemplate}}</div>');
        this.template.add('        </td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td align="center" ><input type="button" value="OK" onclick="com_yung_handsontable_HandsontableFilter.instance(\'{{id}}\').executeFilter()" /> &nbsp; &nbsp; <input type="button" value="Cancel" onclick="com_yung_handsontable_HandsontableFilter.instance(\'{{id}}\').hideFilter()" /></td>');
        this.template.add('    </tr>');
        this.template.add('</table>');
        this.optTemplate = new com.yung.util.BasicTemplate({});
        this.optTemplate.add('<div style="display: block;"> <input name="handsontable-filter-checkbox" data="{{rowIdxs}}" type="checkbox" value="{{value}}"> {{shortVal}} </div>');
        this.colIdx = null;
        return this;
    },
    setupFilter : function () {
        var maxColIdx = this.hsontUtil.getLastValidColumnIdx(0);
        if (maxColIdx <= 0) {
            return;
        }
        var maxRowIdx = this.hsontUtil.getLastValidRowIdx(1, maxColIdx);
        if (maxRowIdx <= 0) {
            return;
        }
        this.hsontUtil.filter = true;
        this.hsontUtil.freeze(1, 0);
        this.hsontUtil.lockRow(0);
        this.hsontUtil.render();
    },
    cancelFilter : function () {
        this.hsontUtil.filter = false;
        this.hsontUtil.filterColSet.clear();
        this.hsontUtil.freeze(0, 0);
        this.hsontUtil.unlockRow(0);
        this.hideFilter();
        if (this.srcData["data"] != null) {
            var dirtyArray = this.hsontUtil.dirtyRowSet.toArray();
            if (dirtyArray.length > 0) {
                var currentData = this.hsontUtil.getData();
                var rowIdMap = this.filterData["rowIdMap"];
                for (var i = 0; i < dirtyArray.length; i++) {
                    var dirtyRow = dirtyArray[i];
                    var srcRow = rowIdMap.get(dirtyRow);
                    this.srcData["data"][srcRow] = currentData[dirtyRow];
                    this.srcData["dirtyRowSet"].add(srcRow);
                }
            }
            this.hsontUtil.setData(this.srcData["data"]);
            if (this.srcData["dirtyRowSet"] != null) {
                this.hsontUtil.dirtyRowSet.addAll(this.srcData["dirtyRowSet"]);
            }
            if (this.srcData["readRowSet"] != null) {
                this.hsontUtil.readRowSet.addAll(this.srcData["readRowSet"]);
            }
            if (this.srcData["readColSet"] != null) {
                this.hsontUtil.readColSet.addAll(this.srcData["readColSet"]);
            }
            if (this.srcData["lockCellMap"] != null) {
                this.hsontUtil.lockCellMap.putAll(this.srcData["lockCellMap"]);
            }
            if (this.srcData["cellColorMap"] != null) {
                this.hsontUtil.cellColorMap.putAll(this.srcData["cellColorMap"]);
            }
            if (this.srcData["rowColorMap"] != null) {
                this.hsontUtil.rowColorMap.putAll(this.srcData["rowColorMap"]);
            }
            if (this.srcData["rowBorderMap"] != null) {
                this.hsontUtil.rowBorderMap.putAll(this.srcData["rowBorderMap"]);
            }
        }
        this.srcData = {};
        this.filterData = {};
        this.hsontUtil.render();
    },
    showFilter : function (colIdx, img) {
        this.colIdx = colIdx;
        var td = jQuery(img).parent();
        var pos = new com.yung.util.Position(td).getBotLeftPosition();
        var tableHtml = this.getFilterHtml(colIdx);
        this.filterDiv.openFloatDiv(tableHtml, pos, 200, 335);
    },
    hideFilter : function () {
        this.filterDiv.showFloatDiv(false);
    },
    getFilterHtml : function (colIdx) {
        var colArray = this.hsontUtil.hot.getDataAtCol(colIdx);
        var valueMap = new com.yung.util.Map('string', com.yung.util.BasicSet);
        for (var i = 1; i < colArray.length; i++) {
            if (colArray[i] != null && colArray[i] != '') {
                var key = this.getEscapeHtmlText(colArray[i]);
                if (key != '') {
                    var rowIdSet = valueMap.get(key);
                    if (rowIdSet == null) {
                        rowIdSet = new com.yung.util.BasicSet('number');
                    }
                    rowIdSet.add(i);
                    valueMap.put(key, rowIdSet);
                }
            }
        }
        var optTemplateHtml = '';
        var tempArray = valueMap.getKeyArray();
        for (var i = 0; i < tempArray.length; i++) {
            var val = tempArray[i];
            var shortVal = val;
            if (val.length > 15) {
                shortVal = val.substring(0, 15) + "...";
            }
            this.optTemplate.data['value'] = val;
            this.optTemplate.data['shortVal'] = shortVal;
            var rowIdArray = valueMap.get(val + '').toArray();
            this.optTemplate.data['rowIdxs'] = JSON.stringify(rowIdArray);
            optTemplateHtml = optTemplateHtml + this.optTemplate.toHtml();
        }
        this.template.data["colIdx"] = colIdx + "";
        this.template.data["optTemplate"] = optTemplateHtml;
        return this.template.toHtml();
    },
    getEscapeHtmlText : function (value) {
        if (value != null) {
            if (value.indexOf(">") >= 0 || value.indexOf("<") >= 0) {
                value = jQuery(value).text();
            }
        }
        return value;
    },
    prepareCache : function (currentData) {
        if (this.srcData["data"] == null) {
            this.srcData["data"] = currentData;
        } else {
            var dirtyArray = this.hsontUtil.dirtyRowSet.toArray();
            if (dirtyArray.length > 0) {
                var rowIdMap = this.filterData["rowIdMap"];
                for (var i = 0; i < dirtyArray.length; i++) {
                    var dirtyRow = dirtyArray[i];
                    var srcRow = rowIdMap.get(dirtyRow);
                    this.srcData["data"][srcRow] = currentData[dirtyRow];
                    this.srcData["dirtyRowSet"].add(srcRow);
                }
            }
        }
        if (this.srcData["dirtyRowSet"] == null) {
            this.srcData["dirtyRowSet"] = new com.yung.util.Set("number");
            this.srcData["dirtyRowSet"].addAll(this.hsontUtil.dirtyRowSet);
        }
        if (this.srcData["readRowSet"] == null) {
            this.srcData["readRowSet"] = new com.yung.util.Set("number");
            this.srcData["readRowSet"].addAll(this.hsontUtil.readRowSet);
        }
        if (this.srcData["readColSet"] == null) {
            this.srcData["readColSet"] = new com.yung.util.Set("number");
            this.srcData["readColSet"].addAll(this.hsontUtil.readColSet);
        }
        if (this.srcData["lockCellMap"] == null) {
            this.srcData["lockCellMap"] = new com.yung.handsontable.CellGroupMap();
            this.srcData["lockCellMap"].putAll(this.hsontUtil.lockCellMap);
        }
        if (this.srcData["cellColorMap"] == null) {
            this.srcData["cellColorMap"] = new com.yung.handsontable.CellGroupMap();
            this.srcData["cellColorMap"].putAll(this.hsontUtil.cellColorMap);
        }
        if (this.srcData["rowColorMap"] == null) {
            this.srcData["rowColorMap"] = new com.yung.util.Map("number", "string");
            this.srcData["rowColorMap"].putAll(this.hsontUtil.rowColorMap);
        }
        if (this.srcData["rowBorderMap"] == null) {
            this.srcData["rowBorderMap"] = new com.yung.util.Map("number", "string");
            this.srcData["rowBorderMap"].putAll(this.hsontUtil.rowBorderMap);
        }
    },
    updateFilterCache : function (srcData, oldRowIdMap, filterCache, newRowIdx, oldRowIdx) {
        var ret = {};
        var newRowIdMap = filterCache["newRowIdMap"];
        var newLockCellMap = filterCache["newLockCellMap"];
        var newReadRowSet = filterCache["newReadRowSet"];
        var newCellColorMap = filterCache["newCellColorMap"];
        var newRowColorMap = filterCache["newRowColorMap"];
        var newRowBorderMap = filterCache["newRowBorderMap"];
        if (oldRowIdMap == null) {
            newRowIdMap.put(newRowIdx, oldRowIdx);
            if (srcData["lockCellMap"] != null) {
                var cellMap = srcData["lockCellMap"].getCellMap(oldRowIdx);
                if (cellMap != null) {
                    newLockCellMap.setCellMap(newRowIdx, cellMap);
                }
            }
            if (srcData["readRowSet"] != null) {
                if (srcData["readRowSet"].contains(oldRowIdx)) {
                    newReadRowSet.add(newRowIdx);
                }
            }
            if (srcData["cellColorMap"] != null) {
                var cellMap = srcData["cellColorMap"].getCellMap(oldRowIdx);
                if (cellMap != null) {
                    newCellColorMap.setCellMap(newRowIdx, cellMap);
                }
            }
            if (srcData["rowColorMap"] != null) {
                var rowColor = srcData["rowColorMap"].get(oldRowIdx);
                if (rowColor != null) {
                    newRowColorMap.put(newRowIdx, rowColor);
                }
            }
            if (srcData["rowBorderMap"] != null) {
                var rowBorder = srcData["rowBorderMap"].get(oldRowIdx);
                if (rowBorder != null) {
                    newRowBorderMap.put(newRowIdx, rowBorder);
                }
            }
        } else {
            newRowIdMap.put(newRowIdx, oldRowIdMap.get(oldRowIdx));
            if (srcData["lockCellMap"] != null) {
                var cellMap = srcData["lockCellMap"].getCellMap(oldRowIdMap.get(oldRowIdx));
                if (cellMap != null) {
                    newLockCellMap.setCellMap(newRowIdx, cellMap);
                }
            }
            if (srcData["readRowSet"] != null) {
                if (srcData["readRowSet"].contains(oldRowIdMap.get(oldRowIdx))) {
                    newReadRowSet.add(newRowIdx);
                }
            }
            if (srcData["cellColorMap"] != null) {
                var cellMap = srcData["cellColorMap"].getCellMap(oldRowIdMap.get(oldRowIdx));
                if (cellMap != null) {
                    newCellColorMap.setCellMap(newRowIdx, cellMap);
                }
            }
            if (srcData["rowColorMap"] != null) {
                var rowColor = srcData["rowColorMap"].get(oldRowIdMap.get(oldRowIdx));
                if (rowColor != null) {
                    newRowColorMap.put(newRowIdx, rowColor);
                }
            }
            if (srcData["rowBorderMap"] != null) {
                var rowBorder = srcData["rowBorderMap"].get(oldRowIdMap.get(oldRowIdx));
                if (rowBorder != null) {
                    newRowBorderMap.put(newRowIdx, rowBorder);
                }
            }
        }
    },
    sort : function (colIdx, order) {
        var currentData = this.hsontUtil.getData();
        this.prepareCache(currentData);
        var maxColIdx = this.hsontUtil.getLastValidColumnIdx(0);
        var maxRowIdx = this.hsontUtil.getLastValidRowIdx(1, maxColIdx);
        var colArray = this.hsontUtil.getDataAtCol(colIdx);
        var treeMap = null;
        var stringTreeMap = null;
        var keyType = "string";
        if (this.hsontUtil.getColumnType(colIdx) == "numeric") {
            keyType = "number";
        }
        if (order == 'asc') {
            treeMap = new com.yung.util.TreeMap(keyType, com.yung.util.BasicSet);
            stringTreeMap = new com.yung.util.TreeMap("string", com.yung.util.BasicSet);
        } else if (order == 'desc') {
            treeMap = new com.yung.util.TreeMap(keyType, com.yung.util.BasicSet, true);
            stringTreeMap = new com.yung.util.TreeMap("string", com.yung.util.BasicSet, true);
        } else {
            throw "unknown order: " + order;
        }
        for (var i = 1; i < colArray.length; i++) {
            var val = colArray[i];
            if (val == null) {
                val = '';
            }
            if (jQuery.isNumeric(val) == true) {
                val = val * 1.0;
            } else {
                val = val + "";
            }
            if (i > maxRowIdx) {
                continue;
            }
            if (keyType == 'number' && typeof val == 'string') {
                var set = stringTreeMap.get(val);
                if (set == null) {
                    set = new com.yung.util.BasicSet("number");
                }
                set.add(i);
                stringTreeMap.put(val, set);
            } else {
                if (keyType == 'string') {
                    val = val + "";
                }
                var set = treeMap.get(val);
                if (set == null) {
                    set = new com.yung.util.BasicSet("number");
                }
                set.add(i);
                treeMap.put(val, set);
            }
        }
        var newData = [];
        newData.push(currentData[0]);
        var newRowIdx = 1;
        var oldRowIdMap = this.filterData["rowIdMap"];
        var filterCache = {};
        filterCache["newRowIdMap"] = new com.yung.util.TreeMap('number', 'number');
        filterCache["newReadRowSet"] = new com.yung.util.BasicSet("number");
        filterCache["newLockCellMap"] = new com.yung.handsontable.CellGroupMap();
        filterCache["newCellColorMap"] = new com.yung.handsontable.CellGroupMap();
        filterCache["newRowColorMap"] = new com.yung.util.Map("number", "string");
        filterCache["newRowBorderMap"] = new com.yung.util.Map("number", "string");
        var newDirtyRowSet = new com.yung.util.BasicSet("number");
        var keyArray = treeMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var key = keyArray[i];
            var idxArray = treeMap.get(key).toArray();
            for (var j = 0; j < idxArray.length; j++) {
                var oldRowIdx = idxArray[j];
                this.updateFilterCache(this.srcData, oldRowIdMap, filterCache, newRowIdx, oldRowIdx);
                newData.push(currentData[oldRowIdx]);
                newDirtyRowSet.add(newRowIdx);
                newRowIdx++;
            }
        }
        var strKeyArray = stringTreeMap.getKeyArray();
        for (var i = 0; i < strKeyArray.length; i++) {
            var key = strKeyArray[i];
            var idxArray = stringTreeMap.get(key).toArray();
            for (var j = 0; j < idxArray.length; j++) {
                var oldRowIdx = idxArray[j];
                this.updateFilterCache(this.srcData, oldRowIdMap, filterCache, newRowIdx, oldRowIdx);
                newData.push(currentData[oldRowIdx]);
                newDirtyRowSet.add(newRowIdx);
                newRowIdx++;
            }
        }
        var cacheReadColSet = new com.yung.util.BasicSet("number");
        cacheReadColSet.addAll(this.hsontUtil.readColSet); // keep lock column only for edit mode
        var cacheHiddenColumns = new com.yung.util.BasicSet("number");
        cacheHiddenColumns.addByArray(this.hsontUtil.hot._hiddenColumns); // keep hidden column only for edit mode
        this.hsontUtil.setData(newData);
        this.filterData["data"] = newData;
        this.filterData["rowIdMap"] = filterCache["newRowIdMap"];
        this.hsontUtil.lockCellMap.putAll(filterCache["newLockCellMap"]);
        this.hsontUtil.readRowSet.addAll(filterCache["newReadRowSet"]);
        this.hsontUtil.cellColorMap = filterCache["newCellColorMap"];
        this.hsontUtil.rowColorMap = filterCache["newRowColorMap"];
        this.hsontUtil.rowBorderMap = filterCache["newRowBorderMap"];
        this.hsontUtil.readColSet.addAll(cacheReadColSet); // keep lock column only for edit mode
        this.hsontUtil.hot._hiddenColumns = cacheHiddenColumns.toArray(); // keep hidden column only for edit mode
        this.hsontUtil.dirtyRowSet.addAll(newDirtyRowSet); // keep dirty rows
        this.hideFilter();
        this.hsontUtil.render();
    },
    checkAll : function () {
        this.uncheckAll();
        jQuery("input[name='handsontable-filter-checkbox']").each(function( index ) {
            var parentDiv = jQuery(this).parent();
            var display = parentDiv.css("display");
            if (display == 'block') {
                jQuery(this).prop("checked", true);
            }
        });
    },
    uncheckAll : function () {
        jQuery("input[name='handsontable-filter-checkbox']").each(function( index ) {
            jQuery(this).prop("checked", false);
        });
    },
    filterOptions : function (input) {
        if (input == null) {
            input = jQuery("#handsontable-filter-" + this.colIdx).get(0);
        }
        this.uncheckAll();
        var criteria = jQuery(input).val();
        var condition = jQuery("#handsontable-filter-condition-" + this.colIdx).val();
        var self = this;
        jQuery("input[name='handsontable-filter-checkbox']").each(function( index ) {
        	var rawVal = $(this).val();
            var val = self.getEscapeHtmlText(rawVal);
            var parentDiv = jQuery(this).parent();
            if (criteria === '') {
                parentDiv.css("display", "block");
            } else {
                var show = "block";
                if (condition == "include") {
                    var include = val.indexOf(criteria) >= 0;
                    if (include == false) {
                        show = "none";
                    }
                } else if (condition == "exclude") {
                    var include = val.indexOf(criteria) >= 0;
                    if (include == true) {
                        show = "none";
                    }
                } else if (condition == "equal") {
                    if (criteria != val) {
                        show = "none";
                    }
                } else if (condition == "not equal") {
                    if (criteria === val) {
                        show = "none";
                    }
                } else if (condition == "greater") {
                    if (jQuery.isNumeric(val) && jQuery.isNumeric(criteria)) {
                        val = val * 1.0;
                        criteria = criteria * 1.0;
                    }
                    if (val <= criteria) {
                        show = "none";
                    }
                } else if (condition == "less") {
                    if (jQuery.isNumeric(val) && jQuery.isNumeric(criteria)) {
                        val = val * 1.0;
                        criteria = criteria * 1.0;
                    }
                    if (val >= criteria) {
                        show = "none";
                    }
                }
                parentDiv.css("display", show);
            }
        });
    },
    executeFilter : function () {
        var currentData = this.hsontUtil.getData();
        this.prepareCache(currentData);
        var oldRowIdMap = this.filterData["rowIdMap"];
        var newData = [];
        newData.push(currentData[0]);
        var newRowIdx = 1;
        var self = this;
        var filterCache = {};
        filterCache["newRowIdMap"] = new com.yung.util.TreeMap('number', 'number');
        filterCache["newReadRowSet"] = new com.yung.util.BasicSet("number");
        filterCache["newLockCellMap"] = new com.yung.handsontable.CellGroupMap();
        filterCache["newCellColorMap"] = new com.yung.handsontable.CellGroupMap();
        filterCache["newRowColorMap"] = new com.yung.util.Map("number", "string");
        filterCache["newRowBorderMap"] = new com.yung.util.Map("number", "string");
        var newDirtyRowSet = new com.yung.util.BasicSet("number");
        jQuery("input[name='handsontable-filter-checkbox']").each(function( index ) {
            var checked = jQuery(this).prop("checked");
            if (checked == true) {
                var data = jQuery(this).attr("data");
                var rowArray = JSON.parse(data);
                for (var i = 0; i < rowArray.length; i++) {
                    var oldRowIdx = rowArray[i];
                    self.updateFilterCache(self.srcData, oldRowIdMap, filterCache, newRowIdx, oldRowIdx);
                    newData.push(currentData[rowArray[i]]);
                    if (self.hsontUtil.dirtyRowSet.contains(oldRowIdx)) {
                    	newDirtyRowSet.add(newRowIdx);
                    }
                    newRowIdx++;
                }
            }
        });
        this.hsontUtil.filterColSet.add(this.colIdx);
        var cacheReadColSet = new com.yung.util.BasicSet("number");
        cacheReadColSet.addAll(this.hsontUtil.readColSet); // keep lock column only for edit mode
        var cacheHiddenColumns = new com.yung.util.BasicSet("number");
        cacheHiddenColumns.addByArray(this.hsontUtil.hot._hiddenColumns); // keep hidden column only for edit mode
        this.hsontUtil.setData(newData);
        this.filterData["data"] = newData;
        this.filterData["rowIdMap"] = filterCache["newRowIdMap"];
        this.hsontUtil.lockCellMap.putAll(filterCache["newLockCellMap"]);
        this.hsontUtil.readRowSet.addAll(filterCache["newReadRowSet"]);
        this.hsontUtil.cellColorMap = filterCache["newCellColorMap"];
        this.hsontUtil.rowColorMap = filterCache["newRowColorMap"];
        this.hsontUtil.rowBorderMap = filterCache["newRowBorderMap"];
        this.hsontUtil.readColSet.addAll(cacheReadColSet); // keep lock column only for edit mode
        this.hsontUtil.hot._hiddenColumns = cacheHiddenColumns.toArray(); // keep hidden column only for edit mode
        this.hsontUtil.dirtyRowSet.addAll(newDirtyRowSet); // keep dirty rows
        this.hsontUtil.render();
        this.setupFilter();
        this.hideFilter();
    },
    keepLockColumn : function () {
        
        
    }
});

com_yung_handsontable_HandsontableFilter.instance = function (filter) {
    if (filter != null) {
        if (filter instanceof com_yung_handsontable_HandsontableFilter) {
            yung_global_var["com_yung_handsontable_HandsontableFilter-" + filter.id] = filter;
        }
        if (typeof filter == 'string') {
            var instance = yung_global_var["com_yung_handsontable_HandsontableFilter-" + filter];
            return instance;
        }
    }
}
/** 
 * require 
 */
jQuery( document ).ready(function() {
    if (typeof com_yung_handsontable_HandsontableUtil == 'undefined') {
        alert("yung-HandsontableFilter.js requires yung-HandsontableUtil.js!");
    }
    if (typeof com_yung_util_Collection == 'undefined') {
        alert("yung-HandsontableFilter.js requires yung-Collection.js!");
    }
    if (typeof com_yung_util_Map == 'undefined') {
        alert("yung-HandsontableFilter.js requires yung-Map.js!");
    }
    if (typeof com_yung_util_FloatDiv == 'undefined') {
        alert("yung-HandsontableFilter.js requires yung-FloatDiv.js!");
    }
    if (typeof com_yung_util_BasicTemplate == 'undefined') {
        alert("yung-HandsontableFilter.js requires yung-BasicTemplate.js!");
    }
    if (typeof com_yung_util_Position == 'undefined') {
        alert("yung-HandsontableFilter.js requires yung-Position.js!");
    }
});