/**
 * handsontable utility
 * 
 */
var com_yung_handsontable_HandsontableUtil = $Class.extend({
    classProp : { 
        name : "com.yung.handsontable.HandsontableUtil" , 
        selectPng : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwQAADsEBuJFr7QAAABZ0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMDvo9WkAAABUSURBVChTYwCC/0RiCEEAkKGwoaHhPzYMkgMCiMK8vDwQAysuLS2FsSGEn58fXBKGg4ODwXIgNpwAAagEuiIQQFUIAiBnQJ2CDDAV4gBwhQQww38ALWdP1YmBkgUAAAAASUVORK5CYII=",
        filterPng : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABZ0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMDvo9WkAAABiSURBVChTYwCCx0D8Hwe+DcQo4AUQoyt6BMRYAbJinIpgAKaQICBKIRMQE6WQC4g/QWmCwBCImZydnS8yMzODTWdnZ/8fHh5+HCyLDfDw8FxmYmL6LyoquhgqhBdch1AMDAA/siKyg6ZPUQAAAABJRU5ErkJggg=="
    },
    debug : true,
    hot : null,
    columnIdMap : null,
    selection : null,
    container : null,
    containerId : null,
    visibleRows : null,
    cellColorMap : null,
    colColorMap : null,
    rowColorMap : null,
    colTypeMap : null,
    rowBorderMap : null,
    colBorderMap : null,
    freezeRow : null,
    freezeCol : null,
    firstRowIdx : null,
    lastRowIdx : null,
    firstColIdx : null,
    lastColIdx : null,
    defaultColWidth : null,
    hiddenTbRIdxs : null,
    hiddenTbCIdxs : null,
    defaultColHeader : { 'A' : 0, "B" : 1, "C" : 2, 'D' : 3, "E" : 4, "F" : 5, 'G' : 6, "H" : 7, "I" : 8, 'J' : 9, "K" : 10, "L" : 11, 'M' : 12, "N" : 13, "O" : 14, 'P' : 15, "Q" : 16, "R" : 17, 'S' : 18, "T" : 19, "U" : 20, 'V' : 21, "W" : 22, "X" : 23, 'Y' : 24, "Z" : 25},
    fireRowHook : null,
    fireColHook : null,
    dirtyRowSet : null,
    afterChangeCallback : null,
    readRowSet : null,
    readColSet : null,
    lockCellMap : null,
    lockTable : null,
    filter : null,
    filterColSet : null,
    init: function(hot, container){
        this.hot = hot;
        this.columnIdMap = new com.yung.util.Map("number", "string");
        this.selection = [];
        this.container = container;
        this.containerId = jQuery(container).attr("id");
        var hotHeight = jQuery(container).height();
        this.visibleRows = parseInt((hotHeight / 24) + 1);
        if (this.visibleRows < 30) {
            this.visibleRows = 30;
        }
        this.cellColorMap = new com.yung.handsontable.CellGroupMap();
        this.rowColorMap = new com.yung.util.Map("number", "string");
        this.colColorMap = new com.yung.util.Map("number", "string");
        this.colTypeMap = {};
        this.rowBorderMap = new com.yung.util.Map("number", "string");
        this.colBorderMap = new com.yung.util.Map("number", "string");
        this.freezeRow = 0;
        this.freezeCol = 0;
        this.firstRowIdx = null;
        this.lastRowIdx = null;
        this.firstColIdx = null;
        this.lastColIdx = null;
        this.defaultColWidth = 60;
        this.hiddenTbRIdxs = [];
        this.hiddenTbCIdxs = [];
        this.fireRowHook = false;
        this.fireColHook = false;
        this.dirtyRowSet = new com.yung.util.Set("number");
        this.afterChangeCallback = null;
        this.filter = false;
        this.filterColSet = new com.yung.util.BasicSet("number");
        this.readRowSet = new com.yung.util.BasicSet("number");
        this.readColSet = new com.yung.util.BasicSet("number");
        this.lockCellMap = new com.yung.handsontable.CellGroupMap();
        this.lockTable = false;
        if (hot != null) {
            this.setup();
        }
        return this;
    },
    setup : function () {
        var settings = this.hot.getSettings();
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        var util = this;
        updateSettings["cells"] = function (row, col, prop) {
            this.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                if (util.lockTable === true) {
                    td.style.color = '#262626';
                } else {
                    td.style.color = '';
                }
                if (util.filter == true && row == 0 && value != null && value != '') {
                    var png = "";
                    if (util.filterColSet.contains(col)) {
                        png = util.classProp.filterPng;
                    } else {
                        png = util.classProp.selectPng;
                    }
                    arguments[5] = "<img id='' src='" + png + "' style='cursor: pointer;' onclick='com_yung_handsontable_HandsontableFilter.instance(\"" + util.containerId + "\").showFilter(" + col + ", this)' /> " + value;
                    Handsontable.renderers.HtmlRenderer.apply(this, arguments);
                } else {
                    var colType = util.colTypeMap["" + col];
                    if (colType == null) {
                        Handsontable.renderers.TextRenderer.apply(this, arguments);
                    } else if (colType == "date") {
                        Handsontable.renderers.DateRenderer.apply(this, arguments);
                    } else if (colType == "dropdown") {
                        Handsontable.renderers.DropdownRenderer.apply(this, arguments);
                    } else if (colType == "html") {
                        Handsontable.renderers.HtmlRenderer.apply(this, arguments);
                    } else if (colType == "numeric") {
                        Handsontable.renderers.NumericRenderer.apply(this, arguments);
                    } else if (colType == "checkbox") {
                        if (value != true) {
                            arguments[5] = false;
                        }
                        Handsontable.renderers.CheckboxRenderer.apply(this, arguments);
                    } else if (colType == "autocomplete") {
                        Handsontable.renderers.AutocompleteRenderer.apply(this, arguments);
                    } else if (colType == "base") {
                        Handsontable.renderers.BaseRenderer.apply(this, arguments);
                    } else if (colType == "time") {
                        Handsontable.renderers.TimeRenderer.apply(this, arguments);
                    } else if (colType == "password") {
                        Handsontable.renderers.PasswordRenderer.apply(this, arguments);
                    } else {
                        Handsontable.renderers.TextRenderer.apply(this, arguments);
                    }
                }
                com_yung_handsontable_HandsontableUtil.applyCellColor.apply(util, arguments);
                com_yung_handsontable_HandsontableUtil.applyBorderColor.apply(util, arguments);
            }
            if (util.lockTable === true) {
                this.readOnly = true;
            } else if (util.lockCellMap.getCell(row, col) != null) {
                this.readOnly = true; // make column read-only
            } else if (util.readRowSet.contains(row)) {
                this.readOnly = true; // make row read-only
            } else if (util.readColSet.contains(col)) {
                this.readOnly = true; // make column read-only
            } else {
                this.readOnly = undefined;
            }
        };
        var columns = settings["columns"];
        if (columns == null) {
            columns = [];
            var colCnt = this.hot.countCols();
            for (var i = 0; i < colCnt; i++) {
                var header = this.hot.getColHeader(i);
                var col = {};
                col["data"] = header;
                col["editor"] = "text";
                col["type"] = "text";
                columns.push(col);
            }
        }
        updateSettings["columns"] = columns;
        var colWidths = settings["colWidths"];
        if (colWidths == null) {
            colWidths = [];
            var colCnt = this.hot.countCols();
            for (var i = 0; i < colCnt; i++) {
                colWidths.push(this.defaultColWidth);
            }
        }
        updateSettings["colWidths"] = colWidths;
        var cell = settings["cell"];
        if (cell == null) {
            cell = [];
        }
        updateSettings["cell"] = cell;
        updateSettings["viewportRowRenderingOffset"] = this.visibleRows;
        this.hot._hiddenColumns = [];
        this.hot._hiddenRows = [];
        this.hot.updateSettings(updateSettings);
        var hsontUtil = this;
        this.hot.addHook('afterScrollVertically', function () {
            hsontUtil.reRenderRow();
        });
        this.hot.addHook('afterScrollHorizontally', function () {
            hsontUtil.reRenderCol();
        });
        this.hot.addHook('afterChange', function (changes, source) {
            if (changes != null && changes.length > 0) {
                for (var i = 0; i < changes.length; i++) {
                    var row = changes[i][0];
                    var oldVal = changes[i][2];
                    var newVal = changes[i][3];
                    if (oldVal != newVal) {
                        hsontUtil.dirtyRowSet.add(row);
                        if (typeof hsontUtil.afterChangeCallback == 'function') {
                            hsontUtil.afterChangeCallback(row);
                        }
                    }
                }
            }
        });
        this.hot.addHook('afterSelection', function (r, c, r2, c2) {
            hsontUtil.selection[0] = r;
            hsontUtil.selection[1] = c;
            hsontUtil.selection[2] = r2;
            hsontUtil.selection[3] = r2;
        });  
        jQuery(".wtHolder:first").scrollEnd(function(){
            if (hsontUtil.fireRowHook == true || hsontUtil.fireRowHook == true) {
                hsontUtil.render();
            }
        }, 250);
    },
    setColumnId : function (colIdx, columnId) {
        this.columnIdMap.put(colIdx, columnId);
    },
    getColumnId : function (colIdx) {
        return this.columnIdMap.get(colIdx);
    },
    getColumnIdx : function (columnId) {
        var keyArray = this.columnIdMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var key = keyArray[i];
            if (this.columnIdMap.get(key) == columnId) {
                return key;
            }
        }
        return null;
    },
    setSuspend : function (suspend) {
        if (suspend == true) {
            this.hot.suspendPaint = true;
        } else {
            this.hot.suspendPaint = false;
        }
    },
    render : function() {
        var suspendPaint = false;
        if (this.hot.suspendPaint == true) {
            suspendPaint == true;
        }
        if (suspendPaint == false) {
            this.hot.render();  
        }
    },
    setAfterChangeCallback : function (callback) {
        if (typeof callback == 'function') {
            this.afterChangeCallback = callback;
        }
    },
    createEmptyData : function (rowNum, colNum) {
        var data = [];
        for (var i = 0; i < rowNum; i++) {
            var r = [];
            for (var j = 0; j < colNum; j++) {
                r.push("");
            }
            data.push(r);
        }
        return data;
    },
    getData : function (startRow, startCol, endRow, endCol) {
        if (startRow == null) {
            startRow = 0;
        }
        if (startCol == null) {
            startCol = 0;
        }
        var colNum = this.getColumnNum();
        var rowNum = this.getRowNum();
        if (endRow == null) {
            endRow = rowNum - 1;
        }
        if (endCol == null) {
            endCol = colNum - 1;
        }
        var data = this.hot.getData(startRow, startCol, endRow, endCol);
        return data;
    },
    getDataObj : function (startRow, startCol, columnId) {
        var data = this.getData(startRow, startCol);
        var dataObj = [];
        for (var r = 0; r < data.length; r++) {
            var row = data[r];
            var empty = true;
            var rowData = {};
            rowData["_rowId"] = r;
            for (var c = 0; c < columnId.length; c++) {
                var val = data[r][c];
                if (val != null && val != '') {
                    empty = false;
                    rowData[columnId[c]] = val;
                }
            }
            if (empty == false) {
                dataObj.push(rowData);
            }
        }
        return dataObj;
    },
    getDataJSON : function (startRow, startCol, columnId) {
        var dataObj = this.getDataObj(startRow, startCol, columnId);
        return JSON.stringify(dataObj);
    },
    getDirtyData : function () {
        var dataArray = [];
        var dirtyRowIds = this.getDirtyRowIndexs();
        for (var i = 0; i < dirtyRowIds.length; i++) {
            var rowData = {};
            var rowId = dirtyRowIds[i];
            for (var c = 0; c < this.getColumnNum(); c++) {
                var columnId = this.getColumnId(c);
                var val = this.getCellValue(rowId, c);    
                rowData[columnId] = val;
            }
            dataArray.push(rowData);
        }
        return dataArray;
    },
    clearAll : function () {
    	var data = this.createEmptyData(200, 20);
    	this.setup();
    	this.setData(data);
    	this.render();
    },
    setData : function(data) {
        if (data !=  null) {
            if (jQuery.isArray(data)) {
                if (data.length > 0) {
                    this.filter = false;
                    this.dirtyRowSet.clear();
                    this.lockCellMap.clear();
                    this.readRowSet.clear();
                    this.readColSet.clear();
                    this.cellColorMap.clear();
                    this.rowColorMap.clear();
                    this.rowBorderMap.clear();
                    this.hot._hiddenColumns = [];
                    this.hot._hiddenRows = [];
                    if (jQuery.isArray(data[0])) {
                        this.setArrayData(data);
                    } else {
                        this.setObjectData(data);
                    }
                }
            } else {
                this.dirtyRowSet.clear();
                this.hot.loadData(data);
            }
        }
    },
    setObjectData : function (data) {
        var objSource = this.objectToObjectSource(data);
        this.hot.loadData(objSource);
        this.render();
    },
    setArrayData : function (dataArray) {
        var objSource = this.arrayToObjectSource(dataArray);
        this.hot.loadData(objSource);
        this.render();
    },
    arrayToObjectSource : function (dataObj) {
        var objSource = [];
        var colCnt = dataObj[0].length;
        for (var i = 0; i < dataObj.length; i++) {
            var col = {};
            for (var j = 0; j < colCnt; j++) {
                var header = this.hot.getColHeader(j);
                col[header + ""] = dataObj[i][j];
            }
            objSource.push(col);
        }
        return objSource;
    },
    objectToObjectSource : function (dataObj) {
        var objSource = [];
        var j = 0;
        var col = {};
        for (var key in dataObj[0]) {
            var header = this.hot.getColHeader(j);
            col[header + ""] = key;
            j++;
        }
        objSource.push(col);
        for (var i = 0; i < dataObj.length; i++) {
            j = 0;
            col = {};
            for (var key in dataObj[i]) {
                var header = this.hot.getColHeader(j);
                col[header + ""] = dataObj[i][key];
                j++;
            }
            objSource.push(col);
        }
        return objSource;
    },
    setCellValue : function (rowIdx, colIdx, value) {
        this.hot.setDataAtCell(rowIdx, colIdx, value);
    },
    getCellValue : function (rowIdx, colIdx) {
        return this.hot.getDataAtCell(rowIdx, colIdx);
    },
    getColumnNum : function () {
        return this.hot.countCols();
    },
    getRowNum : function () {
        return this.hot.countRows();
    },
    lockRow : function (rowIdx) {
        if (this.filter === true) {
            if (this.debug == true) console.log("Lock row is disable when filter is on!");
            return;
        }
        this.readRowSet.add(rowIdx);
        this.render();
    },
    unlockRow : function (rowIdx) {
        if (this.filter === true) {
            if (this.debug == true) console.log("unLock row is disable when filter is on!");
            return;
        }
        this.readRowSet.remove(rowIdx);
        this.render();
    },
    lockColumn : function (colIdx) {
        this.readColSet.add(colIdx);
        this.render();
    },
    unlockColumn : function (colIdx) {
        this.readColSet.remove(colIdx);
        this.render();
    },
    lockCell : function (rowIdx, colIdx) {
        if (this.filter === true) {
            if (this.debug == true) console.log("Lock cell is disable when filter is on!");
            return;
        }
        this.lockCellMap.addCell(rowIdx, colIdx, "readOnly");
        this.render();
    },
    unlockCell : function (rowIdx, colIdx) {
        if (this.filter === true) {
            if (this.debug == true) console.log("unLock cell is disable when filter is on!");
            return;
        }
        this.lockCellMap.removeCell(rowIdx, colIdx);
        this.render();
    },
    lockAll : function() {
        if (this.filter === true) {
            if (this.debug == true) console.log("Lock is disable when filter is on!");
            return;
        }
        this.lockTable = true;
        this.render();
    },
    unlockAll : function() {
        if (this.filter === true) {
            if (this.debug == true) console.log("Lock is disable when filter is on!");
            return;
        }
        this.lockTable = false;
        this.lockCellMap.clear();
        this.readColSet.clear();
        this.readRowSet.clear();
        this.render();
    },
    setColumnType : function (colIdx, type, source) {
        if (source == null) {
            source = [];
        }
        if (type == "text") {
            this.colTypeMap[colIdx + ""] = "text";
        } else if (type == 'date') {
            this.setCalendarColumn(colIdx);
        } else if (type == 'checkbox') {
            this.colTypeMap[colIdx + ""] = "checkbox";
        } else if (type == 'numeric') {
            this.colTypeMap[colIdx + ""] = "numeric";
        } else if (type == 'html') {
            this.colTypeMap[colIdx + ""] = "html";
        } else if (type == 'dropdown') {
            this.setDropdownColumn(colIdx, source);
        } else if (type == 'autocomplete') {
            this.setAutoCompleteColumn(colIdx, source);
        } else {
            alert("unknown type: " + type);
            return;
        }
    },
    setDropdownColumn : function (colIdx, source) {
        this.colTypeMap[colIdx + ""] = "dropdown";
        var settings = this.hot.getSettings();
        var columns = settings["columns"];
        columns[colIdx]["type"] = "dropdown";
        columns[colIdx]["source"] = source;
        columns[colIdx]["editor"] = "dropdown";
        settings["columns"] = columns;
        this.hot.updateSettings(settings);
        this.render();
    },
    setAutoCompleteColumn : function (colIdx, source) {
        this.colTypeMap[colIdx + ""] = "autocomplete";
        var settings = this.hot.getSettings();
        var columns = settings["columns"];
        columns[colIdx]["type"] = "autocomplete";
        columns[colIdx]['editor'] = 'autocomplete';
        columns[colIdx]["source"] = source;
        columns[colIdx]["strict"] = true;
        settings["columns"] = columns;
        this.hot.updateSettings(settings);
        this.render();
    },
    setCalendarColumn : function (colIdx, source) {
        this.colTypeMap[colIdx + ""] = "date";
        var settings = this.hot.getSettings();
        var columns = settings["columns"];
        columns[colIdx]['type'] = 'date';
        columns[colIdx]['editor'] = 'date';
        columns[colIdx]['dateFormat'] = 'MM/DD/YYYY';
        columns[colIdx]['correctFormat'] = true;
        settings["columns"] = columns;
        this.hot.updateSettings(settings);
        this.render();
    },
    getColumnType : function (colIdx) {
        var type = this.colTypeMap[colIdx + ""];
        if (type == null) {
            return "text";
        } else {
            return type;
        }
    },
    setColumnWidth : function (colIdx, width) {
        var settings = this.hot.getSettings();
        var colWidths = settings["colWidths"];
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        if (jQuery.isArray(colIdx) && jQuery.isArray(width)) {
            for (var i = 0; i < colIdx.length; i++) {
                colWidths[colIdx[i]] = width[i];
            }
        } else {
            colWidths[colIdx] = width;
        }
        updateSettings["colWidths"] = colWidths;
        this.hot.updateSettings(updateSettings);
    },
    hideColumn : function (colIdx) {
        if (this.firstColIdx == null || this.lastColIdx == null) {
            this.fetchTableColIndex();
        }
        if (jQuery.isArray(colIdx)) {
            var col = [];
            var colWidth = [];
            for (var i = 0; i < colIdx.length; i++) {
                var id = jQuery.inArray(colIdx[i], this.hot._hiddenColumns);
                if (id < 0) {
                    col.push(colIdx[i]);
                    colWidth.push(0.01);
                    if (this.hot._hiddenColumns.length == 0) this.hookColScroll(true);
                    this.hot._hiddenColumns.push(colIdx[i]);
                    if (colIdx[i] < this.firstColIdx || colIdx[i] > this.lastColIdx) {
                        continue;
                    } else {
                        var cIdx = colIdx[i] - this.firstColIdx;
                        var div = jQuery('table.htCore:eq(1) th:eq(' + cIdx + ') div:first-child');
                        div.css("border-right", "2px solid black");
                        this.hiddenTbCIdxs.push(cIdx);
                    }
                }
            }
            this.setColumnWidth(col, colWidth);
            this.render();
        } else {
            var id = jQuery.inArray(colIdx, this.hot._hiddenColumns);
            if (id < 0) {
                this.setColumnWidth(colIdx, 0.01);
                if (colIdx >= this.firstColIdx && colIdx <= this.lastColIdx) {
                    var cIdx = colIdx - this.firstColIdx;
                    var div = jQuery('table.htCore:eq(1) th:eq(' + cIdx + ') div:first-child');
                    div.css("border-right", "2px solid black");
                    this.hiddenTbCIdxs.push(cIdx);
                }
                this.render();
                if (this.hot._hiddenColumns.length == 0) this.hookColScroll(true);
                this.hot._hiddenColumns.push(colIdx);
            }
        }
    },
    arrayRemove : function remove(array, element) {
        var index = array.indexOf(element);
        if (index !== -1) {
            array.splice(index, 1);
        }
    },
    showColumn : function (colIdx) {
        if (this.firstColIdx == null || this.lastColIdx == null) {
            this.fetchTableColIndex();
        }
        if (jQuery.isArray(colIdx)) {
            var col = [];
            var colWidth = [];
            for (var i = 0; i < colIdx.length; i++) {
                var id = jQuery.inArray(colIdx[i], this.hot._hiddenColumns);
                if (id >= 0) {
                    col.push(colIdx[i]);
                    colWidth.push(this.defaultColWidth);
                    this.arrayRemove(this.hot._hiddenColumns, colIdx[i]);
                    if (colIdx[i] < this.firstColIdx || colIdx[i] > this.lastColIdx) {
                        continue;
                    } else {
                        var cIdx = colIdx[i] - this.firstColIdx;
                        var div = jQuery('table.htCore:eq(1) th:eq(' + cIdx + ') div:first-child');
                        div.css("border-right", "");
                        this.arrayRemove(this.hiddenTbCIdxs, cIdx);
                    }
                }
            }
            if (this.hot._hiddenColumns.length == 0) this.hookColScroll(false);
            this.setColumnWidth(col, colWidth);
            this.render();
        } else {
            var id = jQuery.inArray(colIdx, this.hot._hiddenColumns);
            if (id >= 0) {
                this.setColumnWidth(colIdx, this.defaultColWidth);
                if (colIdx >= this.firstColIdx && colIdx <= this.lastColIdx) {
                    var cIdx = colIdx - this.firstColIdx;
                    var div = jQuery('table.htCore:eq(1) th:eq(' + cIdx + ') div:first-child');
                    div.css("border-right", "");
                    this.arrayRemove(this.hiddenTbCIdxs, cIdx);
                }
                this.render();
                this.arrayRemove(this.hot._hiddenColumns, colIdx);
                if (this.hot._hiddenColumns.length == 0) this.hookColScroll(false);
            }
        }
    },
    showAllColumn : function () {
        this.setSuspend(true);
        this.fetchTableRowIndex();
        if (this.hot._hiddenColumns.length > 0) {
            var col = [];
            var colWidth = [];
            var tempArray = [];
            for (var i = 0; i < this.hot._hiddenColumns.length; i++) {
                tempArray.push(this.hot._hiddenColumns[i]);
            }
            for (var i = 0; i < tempArray.length; i++) {
                var hiddenColIdx = tempArray[i];
                col.push(hiddenColIdx);
                colWidth.push(this.defaultColWidth);
                this.arrayRemove(this.hot._hiddenColumns, hiddenColIdx);
            }
            this.setColumnWidth(col, colWidth);
            this.render();
        }
        var totalViewCols = this.lastColIdx - this.firstColIdx + 1;
        for (var i = 0; i < totalViewCols; i++) {
            var div = jQuery('table.htCore:eq(1) th:eq(' + i + ') div:first-child');
            div.css("border-right", "");
        }
        this.hiddenTbCIdxs = [];
        this.hot._hiddenColumns = [];
        this.hookColScroll(false);
        this.setSuspend(false);
    },
    setComment : function (rowIdx, colIdx, message) {
        var settings = this.hot.getSettings();
        var cell = settings["cell"];
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        var found = false;
        for (var i = 0; i < cell.length; i++) {
            var obj = cell[i];
            if (obj["row"] == rowIdx && obj["col"] == colIdx) {
                var comment = obj["comment"];
                comment["value"] = message;
                obj["comment"] = comment;
                found = true;
                break;
            }
        }
        if (found != true) {
            var obj = {};
            obj["row"] = rowIdx;
            obj["col"] = colIdx;
            var comment = {};
            comment["value"] = message;
            obj["comment"] = comment;
            cell.push(obj);
        }
        updateSettings["cell"] = cell;
        this.hot.updateSettings(updateSettings);
        this.render();
    },
    removeComment : function (rowIdx, colIdx) {
        var settings = this.hot.getSettings();
        var cell = settings["cell"];
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        for (var i = 0; i < cell.length; i++) {
            var obj = cell[i];
            if (obj["row"] == rowIdx && obj["col"] == colIdx) {
                this.arrayRemove(cell, obj);
                updateSettings["cell"] = cell;
                this.hot.updateSettings(updateSettings);
                this.render();
                break;
            }
        }
    },
    setRowColor : function (rowIdx, color) {
        if (color == null) {
            this.rowColorMap.remove(rowIdx);
            this.render();
            return;
        }
        this.rowColorMap.put(rowIdx, color);
        this.render();
    },
    setColumnColor : function (colIdx, color) {
        if (color == null) {
            this.colColorMap.remove(colIdx);
            this.render();
            return;
        }
        this.colColorMap.put(colIdx, color);
        this.render();
    },
    setCellColor : function (rowIdx, colIdx, color) {
        this.cellColorMap.addCell(rowIdx, colIdx, color);
        this.render();
    },
    setRowBorder : function (rowIdx, styles) {
        if (styles == null) {
            styles = "2px, dashed, red";
        }
        this.setRowBorderStyle(rowIdx, styles);
    },
    setRowBorderStyle : function (rowIdx, styles) {
        // ex. styles = "2px, dashed, blue"
        if (styles == null) {
            this.rowBorderMap.remove(rowIdx);
        } else {
            this.rowBorderMap.put(rowIdx, styles);
        }
        this.render();
    },
    setColumnBorder : function (colIdx, styles) {
        // ex. styles = "2px, dashed, blue"
        if (styles == null) {
            this.colBorderMap.remove(colIdx);
        } else {
            this.colBorderMap.put(colIdx, styles);
        }
        this.render();
    },
    freeze : function (rowIdx, colIdx) {
        var settings = this.hot.getSettings();
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        updateSettings["fixedRowsTop"] = rowIdx;
        updateSettings["fixedColumnsLeft"] = colIdx;
        this.hot.updateSettings(updateSettings);
        this.freezeRow = rowIdx;
        this.freezeCol = colIdx;
        this.render();
    },
    fetchTableRowIndex : function () {
        var firstRow = jQuery('span.rowHeader:first');
        var currentFirstRowIdx = parseInt(firstRow.html());
        if (currentFirstRowIdx == this.firstRowIdx) {
            return false;
        }
        this.firstRowIdx = currentFirstRowIdx;
        var lastRow = jQuery('table.htCore:eq(0) tr:last th div span');
        this.lastRowIdx = parseInt(lastRow.html());
        return true;
    },
    fetchTableColIndex : function () {
        var firstCol = jQuery('span.colHeader:eq(1)');
        var currentFirstColIdx = this.parseColHeader(firstCol.html());
        if (currentFirstColIdx == this.firstColIdx) {
            return false;
        }
        this.firstColIdx = currentFirstColIdx;
        var lastCol = jQuery('table.htCore:eq(0) tr:eq(0) th:last div span');
        var txt = lastCol.html();
        this.lastColIdx = this.parseColHeader(txt);
        return true;
    },
    parseColHeader : function (colHeader) {
        var idArr = [];
        if (colHeader != null && colHeader != '') {
            for (var i = colHeader.length; i > 0; i--) {
                var c = colHeader.charAt(i - 1);
                var idx = this.defaultColHeader[c];
                idArr.push(idx);
            }
            var ret = 0;
            for (var i = 0; i < idArr.length; i++) {
                var pre = 0;
                for (var j = 0; j < i; j++) {
                    pre = (idArr[i] + 1) * 26;
                }
                if (pre == 0) {
                    ret = ret + idArr[i];
                } else {
                    ret = ret + pre;
                }
            }
            return ret;
        }
        return null;
    },
    hideRow : function (rowIdx, updateCache) {
        if (this.firstRowIdx == null || this.lastRowIdx == null) {
            this.fetchTableRowIndex();
        }
        if (updateCache == null) {
            updateCache = true;
        }
        if (rowIdx < this.firstRowIdx) {
            if (updateCache) {
                if (this.hot._hiddenRows.length == 0) this.hookRowScroll(true);
                this.hot._hiddenRows.push(rowIdx);
            }
            return;
        }
        if (rowIdx > this.lastRowIdx) {
            if (updateCache) {
                if (this.hot._hiddenRows.length == 0) this.hookRowScroll(true);
                this.hot._hiddenRows.push(rowIdx);
            }
            return;
        }
        var rIdx = rowIdx - this.firstRowIdx + 2;
        var tr1 = jQuery('table.htCore:eq(2) tr:eq(' + rIdx + ')');
        var tr2 = jQuery('table.htCore:eq(0) tr:eq(' + rIdx + ')');
        var div = jQuery('table.htCore:eq(2) th:eq(' + (rIdx + 1) + ') div:first-child');
        this.hideTableTr(tr1, tr2, div, rIdx);
        if (updateCache) {
            if (this.hot._hiddenRows.length == 0) this.hookRowScroll(true);
            this.hot._hiddenRows.push(rowIdx);
        }
    },
    hideRows : function (rowIdxs) {
        this.fetchTableRowIndex();
        this.setSuspend(true);
        for (var i = 0; i < rowIdxs.length; i++) {
            this.hideRow(rowIdxs[i]);
        }
        this.setSuspend(false);
        //this.updateVisibleRow();
        this.render();
    },
    showRow : function (rowIdx, updateCache) {
        if (this.firstRowIdx == null || this.lastRowIdx == null) {
            this.fetchTableRowIndex();
        }
        if (updateCache == null) {
            updateCache = true;
        }
        if (rowIdx < this.firstRowIdx) {
            if (updateCache) {
                this.arrayRemove(this.hot._hiddenRows, rowIdx);
                if (this.hot._hiddenRows.length == 0) this.hookRowScroll(false);
            }
            return;
        }
        if (rowIdx > this.lastRowIdx) {
            if (updateCache) this.arrayRemove(this.hot._hiddenRows, rowIdx);
            if (this.hot._hiddenRows.length == 0) this.hookRowScroll(false);
            return;
        }
        var rIdx = rowIdx - this.firstRowIdx + 2;
        var tr1 = jQuery('table.htCore:eq(2) tr:eq(' + rIdx + ')');
        var tr2 = jQuery('table.htCore:eq(0) tr:eq(' + rIdx + ')');
        var div = jQuery('table.htCore:eq(2) th:eq(' + (rIdx + 1) + ') div:first-child');
        this.showTableTr(tr1, tr2, div, rIdx);
        if (updateCache) this.arrayRemove(this.hot._hiddenRows, rowIdx);
        if (this.hot._hiddenRows.length == 0) this.hookRowScroll(false);
        //if (this.hot.suspendPaint == false) {
        //    this.updateVisibleRow();
        //}
    },
    updateVisibleRow : function () {
        var rowNum = this.getRowNum();
        var span = this.visibleRows * 2;
        var win = new com.yung.handsontable.Window(rowNum, this.hot._hiddenRows, span);
        var winSize = win.getWinSize();
        if (winSize == span) {
            return;
        }
        var settings = this.hot.getSettings();
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        var size = winSize - this.visibleRows;
        if (size > 100) {
            size = 100;
        }
        updateSettings["viewportRowRenderingOffset"] = size;
        this.hot.updateSettings(updateSettings);
        this.render();
    },
    showTableTr : function (tr1, tr2, div, rIdx) {
        tr1.css("display", "");
        tr2.css("display", "");
        div.css("border-top", "");
        this.arrayRemove(this.hiddenTbRIdxs, rIdx);
    },
    hideTableTr : function (tr1, tr2, div, rIdx) {
        tr1.css("display", "none");
        tr2.css("display", "none");
        div.css("border-top", "2px solid black");
        this.hiddenTbRIdxs.push(rIdx);
    },
    showAllRow : function () {
        this.setSuspend(true);
        this.fetchTableRowIndex();
        var totalViewRows = this.lastRowIdx - this.firstRowIdx + 2;
        for (var i = 0; i < totalViewRows; i++) {
            var tr1 = jQuery('table.htCore:eq(2) tr:eq(' + i + ')');
            var tr2 = jQuery('table.htCore:eq(0) tr:eq(' + i + ')');
            var div = jQuery('table.htCore:eq(2) th:eq(' + (i + 1) + ') div:first-child');
            this.showTableTr(tr1, tr2, div, i);
        }
        this.hiddenTbRIdxs = [];
        this.hot._hiddenRows = [];
        this.hookRowScroll(false);
        this.setSuspend(false);
        //this.updateVisibleRow();
    },
    reRenderRow : function () {
        if (this.hot.suspendPaint == true) {
            return;
        }
        if (this.fireRowHook != true) {
            return;
        }
        var change = this.fetchTableRowIndex();
        if (change == false) {
            return;
        }
        var tempArray = [];
        for (var i = 0; i < this.hiddenTbRIdxs.length; i++) {
            tempArray.push(this.hiddenTbRIdxs[i]);
        }
        for (var i = 0; i < tempArray.length; i++) {
            var tr1 = jQuery('table.htCore:eq(2) tr:eq(' + tempArray[i] + ')');
            var tr2 = jQuery('table.htCore:eq(0) tr:eq(' + tempArray[i] + ')');
            var div = jQuery('table.htCore:eq(2) th:eq(' + (tempArray[i] + 1) + ') div:first-child');
            this.showTableTr(tr1, tr2, div, tempArray[i]);
        }
        for (var i = 0; i < this.hot._hiddenRows.length; i++) {
            var hiddenRowIdx = this.hot._hiddenRows[i];
            this.hideRow(hiddenRowIdx, false);
        }
    },
    reRenderCol : function () {
        if (this.hot.suspendPaint == true) {
            return;
        }
        if (this.fireColHook != true) {
            return;
        }
        var change = this.fetchTableColIndex();
        if (change == false) {
            return;
        }
        var tempArray = [];
        for (var i = 0; i < this.hiddenTbCIdxs.length; i++) {
            tempArray.push(this.hiddenTbCIdxs[i]);
        }
        for (var i = 0; i < tempArray.length; i++) {
            var div = jQuery('table.htCore:eq(1) th:eq(' + tempArray[i] + ') div:first-child');
            div.css("border-right", "");
        }
        this.hiddenTbCIdxs = [];
        for (var i = 0; i < this.hot._hiddenColumns.length; i++) {
            var hiddenColIdx = this.hot._hiddenColumns[i];
            var cIdx = hiddenColIdx - this.firstColIdx;
            var div = jQuery('table.htCore:eq(1) th:eq(' + cIdx + ') div:first-child');
            div.css("border-right", "2px solid black");
            this.hiddenTbCIdxs.push(cIdx);
        }
    },
    hookRowScroll : function (add) {
        if (add == true) {
            this.fireRowHook = true;
        } else {
            this.fireRowHook = false;
        }
    },
    hookColScroll : function (add) {
        if (add == true) {
            this.fireColHook = true;
        } else {
            this.fireColHook = false;
        }
    },
    getSelected : function () {
        var select = this.hot.getSelected(); 
        if (select == null) {
            return this.selection;
        } else {
            return select;
        }
    },
    selectCell : function (rowIdx, colIdx) {
        this.hot.selectCell(rowIdx, colIdx);  
    },
    addRow : function (rowIdx, amount) {
        this.hot.alter('insert_row', rowIdx, amount);
    },
    undo : function () {
        this.hot.undo();
    },
    redo : function () {
        this.hot.redo();
    },
    getDirtyRowIndexs : function () {
        return this.dirtyRowSet.toArray();
    },
    getLastValidColumnIdx : function (rowIdx) {
        var rowArray = this.hot.getDataAtRow(rowIdx);
        for (var i = rowArray.length - 1; i >= 0; i--) {
            var cellVal = rowArray[i];
            if (cellVal != null && cellVal != '') {
                return i;
            }
        }
        return 0;
    },
    getLastValidRowIdx : function (startRowIdx, maxColIdx) {
        var rowNum = this.getRowNum();
        for (var r = startRowIdx; r < rowNum; r++) {
            var emptyRow = true;
            var rowArray = this.hot.getDataAtRow(r);
            for (c = 0; c < maxColIdx; c++) {
                var cellVal = rowArray[c];
                if (cellVal != null && cellVal != '') {
                    emptyRow = false;
                    break;
                }
            }
            if (emptyRow == true) {
                return r - 1;
            }
        }
        return rowNum - 1;
    },
    getDataAtCol : function (colIdx) {
        return this.hot.getDataAtCol(colIdx);
    },
    getDataAtRow : function (rowIdx) {
        return this.hot.getDataAtRow(rowIdx);
    }
});

var com_yung_handsontable_CellGroupMap = $Class.extend({
    classProp : { name : "com.yung.handsontable.CellGroupMap"},
    rowCellMap : null,
    init : function () {
        this.rowCellMap = new com.yung.util.Map("number", com.yung.util.MapInterface);
    },
    getCell : function (rowIdx, colIdx) {
        var cellMap = this.rowCellMap.get(rowIdx);
        if (cellMap == null) {
            return null;
        } else {
            return cellMap.get(colIdx);
        }
    },
    addCell : function (rowIdx, colIdx, value) {
        if (value == null) {
            value = "";
        }
        var cellMap = this.rowCellMap.get(rowIdx);
        if (cellMap == null) {
            cellMap = new com.yung.util.Map("number", "string");
        }
        cellMap.put(colIdx, value);
        this.rowCellMap.put(rowIdx, cellMap);
    },
    removeCell : function (rowIdx, colIdx) {
        var cellMap = this.rowCellMap.get(rowIdx);
        if (cellMap != null) {
            cellMap.remove(colIdx);
        }
    },
    clear : function () {
        this.rowCellMap.clear();
    },
    putAll : function (cellGroupMap) {
        if (cellGroupMap instanceof com_yung_handsontable_CellGroupMap) {
            this.rowCellMap.putAll(cellGroupMap.rowCellMap);
        } else {
            throw "argument type is not " + this.classProp.name;
        }
    },
    getCellMap : function (rowIdx) {
        return this.rowCellMap.get(rowIdx);
    },
    setCellMap : function (rowIdx, cellMap) {
        if (cellMap instanceof com_yung_util_MapInterface) {
            this.rowCellMap.put(rowIdx, cellMap);
        } else {
            throw "cellMap is not com.yung.util.MapInterface";
        }
    }
});

com_yung_handsontable_HandsontableUtil.applyCellColor = function (instance, td, rowIdx, colIdx, prop, value, cellProperties) {
    var rowColor = this.rowColorMap.get(rowIdx);
    var colColor = this.colColorMap.get(colIdx);
    var color = this.cellColorMap.getCell(rowIdx, colIdx);
    if (color != null) {
        td.style.backgroundColor = color;
    } else if (rowColor != null) {
        td.style.backgroundColor = rowColor;
    } else if (colColor != null) {
        td.style.backgroundColor = colColor;
    }
}

com_yung_handsontable_HandsontableUtil.applyBorderColor = function (instance, td, rowIdx, colIdx, prop, value, cellProperties) {
    var rowStyle = this.rowBorderMap.get(rowIdx);
    var colStyle = this.colBorderMap.get(colIdx);
    var freezeRow = this.freezeRow;
    if (rowStyle != null) {
        var styles = rowStyle.split(",");
        jQuery(td).css("border-top", styles[0] + " " + styles[1] + " " + styles[2]);
        jQuery(td).css("border-bottom", styles[0] + " " + styles[1] + " " + styles[2]);
    }
    if (colStyle != null) {
        var styles = colStyle.split(",");
        jQuery(td).css("border-left", styles[0] + " " + styles[1] + " " + styles[2]);
        jQuery(td).css("border-right", styles[0] + " " + styles[1] + " " + styles[2]);
    }
    if (this.freezeRow == (rowIdx + 1)) {
        jQuery(td).css("border-bottom", "1px solid black");
    }
    if (this.freezeCol == (colIdx + 1)) {
        jQuery(td).css("border-right", "1px solid black");
    }
}

jQuery.fn.scrollEnd = function(callback, timeout) {          
  jQuery(this).scroll(function(){
    var $this = jQuery(this);
    if ($this.data('scrollTimeout')) {
      clearTimeout($this.data('scrollTimeout'));
    }
    $this.data('scrollTimeout', setTimeout(callback,timeout));
  });
};


/** 
 * require 
 */
jQuery( document ).ready(function() {
    if (typeof com_yung_util_Collection == 'undefined') {
        alert("yung-handsontableUtil.js requires yung-Collection.js!");
    }
    if (typeof com_yung_handsontable_Window == 'undefined') {
        alert("yung-handsontableUtil.js requires yung-handsontableWin.js!");
    }
});