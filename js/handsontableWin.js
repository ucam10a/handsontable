var com_yung_handsontable_Window = $Class.extend({
    classProp : { name : "com.yung.handsontable.Window"},
    list : null,
    span : null,
    init : function (rowNum, markArray, span) {
        this.list = new com.yung.util.BasicList('number');
        for (var i = 0; i < rowNum; i++) {
            this.list.add(1);
        }
        for (var i = 0; i < markArray.length; i++) {
            this.list.set(markArray[i], 0);
        }
        this.span = span;
    },
    getWinSize : function () {
        var maxWinSize = 0;
        var winSize = 1;
        var startIdx = 0;
        var endIdx = 0;
        var rSpan = 0;
        while (rSpan < this.span) {
            var mark = this.list.get(endIdx);
            if (mark == null) {
                return this.span;
            }
            if (mark == 1) {
                rSpan++;
            }
            endIdx++;
        }
        winSize = endIdx - startIdx;
        maxWinSize = winSize;
        while (endIdx < this.list.size()) {
            startIdx++;
            endIdx++;
            var startMark = this.list.get(startIdx);
            var endMark = this.list.get(endIdx);
            if (startMark == 1 && endMark == 1) {
                // do nothing
            } else if (startMark == 0 && endMark == 1) {
                winSize = winSize -1;
            } else if (startMark == 1 && endMark == 0) {
                winSize = winSize + 1
            } else if (startMark == 0 && endMark == 0) {
                // do nothing
            }
            if (winSize > maxWinSize) {
                maxWinSize = winSize;
            }
        }
        return maxWinSize;
    }
});

/** 
 * require 
 */
jQuery( document ).ready(function() {
    if (typeof com_yung_util_Collection == 'undefined') {
        alert("yung-handsontableUtil.js requires yung-Collection.js!");
    }
});